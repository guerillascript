/* coded by Ketmar // Invisible Vector (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/txt/copying/ for more details.
 */
////////////////////////////////////////////////////////////////////////////////
Components.utils.import("chrome://guerilla-script-jscode/content/startup.js").newWindow(this, this.window);


function Guerilla_OpenPkgManUI (win) {
  let inType = "global:console";
  let uri = "chrome://global/content/console.xul";
  let topWindow = Services.wm.getMostRecentWindow("guerilla:pkgmanui");
  if (topWindow) {
    topWindow.focus();
  } else {
    window.open("chrome://guerilla-script/content/guerilla-pkgman.xul", "_blank", "chrome,extrachrome,centerscreen,resizable,scrollbars");
  }
}
