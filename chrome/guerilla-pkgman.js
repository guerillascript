/* coded by Ketmar // Invisible Vector (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/txt/copying/ for more details.
 */
////////////////////////////////////////////////////////////////////////////////
let {utils:Cu, classes:Cc, interfaces:Ci, results:Cr} = Components;
let addonobj = Cu.import("chrome://guerilla-script-jscode/content/startup.js").getAddonObj();

addonobj.sbox.debuglog("GS pkgman window!");
let uiMain = addonobj.sbox.require("mainjs:ui/pkgman.js");

uiMain.newWindow(window);

function doUpdate () uiMain.doUpdate();
function doRemove () uiMain.doRemove();
function doInstall () uiMain.doInstall();
function doInfo () uiMain.doInfo();


function doClearLog () {
  let el = document.getElementById("messages");
  if (el) el.value = "";
}
