/* coded by Ketmar // Invisible Vector (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/txt/copying/ for more details.
 */
////////////////////////////////////////////////////////////////////////////////
Components.utils.import("chrome://guerilla-script-jscode/content/modules/signals.jsm");

////////////////////////////////////////////////////////////////////////////////
require("utils/utils");
let pkgman = require("pkg/pkgman");


////////////////////////////////////////////////////////////////////////////////
function uptime2str (upt) {
  if (typeof(upt) !== "number" || upt < 1) return "never";
  let now = Math.floor(Date.now()/1000);
  if (upt >= now) return "0 seconds ago";
  let diff = now-upt;
  if (diff < 60) return ""+diff+" seconds ago";
  diff = Math.floor(diff/60); // minutes
  if (diff < 60) return ""+diff+" minutes ago";
  diff = Math.floor(diff/60); // hours
  if (diff < 24) return ""+diff+" hours ago";
  diff = Math.floor(diff/24); // days
  return ""+diff+" days ago";
}


////////////////////////////////////////////////////////////////////////////////
let mainWin = null;
let pkgList = null;
let pkgTree = null;
let treeModel = null;


function getCurrentItem () {
  if (!pkgTree) return null;
  if (treeModel.selection.count !== 1) return null;
  return treeModel.getObjectAt(pkgTree.currentIndex);
}


function setButtonStatus () {
  let pi = getCurrentItem();
  let disabled = (pi ? "false" : "true");
  if (disabled === "false" && pi && pi.updating) disabled = "true";
  let el = mainWin.document.getElementById("btnUpdate");
  if (el) el.setAttribute("disabled", disabled);
  el = mainWin.document.getElementById("btnRemove");
  if (el) el.setAttribute("disabled", disabled);
  el = mainWin.document.getElementById("btnInfo");
  if (el) el.setAttribute("disabled", disabled);
}


function onPagackeManagerSignal (signame, state) {
  if (signame !== null) {
    let el = mainWin.document.getElementById("messages");
    let v = el.value;
    if (v) v += "\n";
    if (signame === "package-manager") {
      v += state.name+" : "+state.phase;
    } else {
      v += state.phase+" : "+state.url;
    }
    el.value = v;
    //el.scrollTop = el.scrollHeight;
    el.selectionStart = el.selectionEnd = v.length;
  }
  if (signame === "package-manager") {
    let pkgid = treeModel.idForName(state.name);
    if (pkgid >= 0) {
      treeModel.setUpdatingForId(pkgid, (state.phase === "started"));
      setButtonStatus();
    }
    if (state.phase === "started") return;
    if (state.phase === "complete") {
      pkgList = null;
    }
    /*
    started
    error
    cancelled
    complete
    */
  }
  // update
  let nplist = pkgman.getPackageList();
  if (pkgList === null || pkgList.lmod !== nplist.lmod) {
    pkgList = nplist;
    for (let pi of pkgList.list) treeModel.addPackage(pi);
  }
  setButtonStatus();
}


////////////////////////////////////////////////////////////////////////////////
/* property sheet data model
 * visible item object
 *   string name
 *   string value
 *   string version
 *   int pkgid
 *   bool updating
 *   int uptime
 */


////////////////////////////////////////////////////////////////////////////////
// creator must set methods:
//  string changeValue (name, info, newValue, oldValue)
//    return either `undefined` (accept), new value or throw
function PkgTreeModel () {
  this.visItems = [];
  this.cachePropsByName = {};
    /* key: name
     * value: index in visItems
     */
  this.treebox = null;

  // tree view model part
  // the total number of rows in the tree (including the offscreen rows)
  Object.defineProperty(this, "rowCount", {get: function () this.visItems.length});
}


PkgTreeModel.prototype.clear = function () {
  this.cachePropsByName = {};
  if (this.treebox && this.visItems.length > 0) this.treebox.rowCountChanged(0, -this.visItems.length);
  this.visItems = [];
};


// add property
//TODO: alpha sort
PkgTreeModel.prototype.addPackage = function (pi) {
  let name = pi.name;
  let pkgid = pi.id;
  let version = pi.version;
  let uptime = pkgman.getLastUpdateTimeForId(pkgid);
  //conlog("addPackage: ", pi, "; uptime=", uptime);

  let vii = this.visItems;

  if (name in this.cachePropsByName) {
    let idx = this.cachePropsByName[name];
    let vo = vii[idx];
    vo.pkgid = pkgid;
    vo.updating = false;
    vo.url = pi.url;
    let uptree = false;
    if (vo.uptime !== uptime) {
      uptree = true;
      vo.uptime = uptime;
      vo.value = uptime2str(uptime);
    }
    if (vo.version !== version) {
      uptree = true;
      vo.version = ""+version;
    }
    if (uptree && this.treebox) this.treebox.invalidateRow(idx);
    return;
  }

  //TODO: binary search
  let insertAt = vii.length;
  for (let [i, o] of Iterator(vii)) if (o.name > name) { insertAt = i; break; }

  // visItems object
  let vo = {
    name: name,
    value: uptime2str(uptime2str),
    pkgid: pkgid,
    updating: false,
    uptime: uptime,
    version: version,
    url: pi.url,
  };

  // fix indexes
  for (let idx = insertAt; idx < vii.length; ++idx) {
    let name = vii[idx].name;
    ++this.cachePropsByName[name];
  }
  // insert visItems object
  vii.splice(insertAt, 0, vo);
  this.cachePropsByName[name] = insertAt;
  if (this.treebox) this.treebox.rowCountChanged(insertAt, 1);
};


////////////////////////////////////////////////////////////////////////////////
PkgTreeModel.prototype.getObjectAt = function (index) (index < 0 || index >= this.visItems.length ? null : this.visItems[index]);

PkgTreeModel.prototype.idForName = function (name) {
  for (let vo of Iterator(this.visItems)) {
    if (vo.name === name) return vo.pkgid;
  }
  return -1;
};

PkgTreeModel.prototype.setUpdatingForId = function (pkgid, updflag) {
  updflag = !!updflag;
  for (let [idx, vo] of Iterator(this.visItems)) {
    if (vo.pkgid === pkgid) {
      if (vo.updating !== updflag) {
        vo.updating = updflag;
        if (this.treebox) this.treebox.invalidateRow(idx);
      }
    }
  }
};


PkgTreeModel.prototype.setLastUpTime = function (pkgid, uptime) {
  let vii = this.visItems;
  for (let [idx, vo] of Iterator(vii)) {
    if (vo.pkgid === pkgid) {
      if (vo.uptime !== uptime) {
        vo.uptime = uptime;
        vo.value = uptime2str(uptime);
        if (this.treebox) this.treebox.invalidateRow(idx);
      }
    }
  }
};


////////////////////////////////////////////////////////////////////////////////
// treeview model
// the total number of rows in the tree (including the offscreen rows)
//Object.defineProperty(this, "rowCount", {get: function () this.visItems.length});

//boolean canDrop(in long index, in long orientation, in nsIDOMDataTransfer dataTransfer);
PkgTreeModel.prototype.canDrop = function (index, orientation, dataTransfer) false;

//void cycleCell(in long row, in nsITreeColumn col);
PkgTreeModel.prototype.cycleCell = function (row, col) {};

//void cycleHeader(in nsITreeColumn col);
PkgTreeModel.prototype.cycleHeader = function (col) {};

//void drop(in long row, in long orientation, in nsIDOMDataTransfer dataTransfer);
PkgTreeModel.prototype.drop = function (row, orientation, dataTransfer) {};

//AString getCellProperties(in long row, in nsITreeColumn col);
// return spalce-delimited string of properties
PkgTreeModel.prototype.getCellProperties = function (row, col) {
  let props = this.getRowProperties(row);
  //if (col.id === "artree-col-flag") props += " favicon";
  return props;
};

//AString getCellText(in long row, in nsITreeColumn col);
PkgTreeModel.prototype.getCellText = function (row, col) {
  let vo = this.getObjectAt(row);
  switch (col.id) {
    case "pkglist_name": return vo.name;
    case "pkglist_update": return vo.value;
    case "pkglist_version": return vo.version;
  }
  return "";
};

//AString getCellValue(in long row, in nsITreeColumn col);
// this method is only called for columns of type other than text

//AString getColumnProperties(in nsITreeColumn col);
PkgTreeModel.prototype.getColumnProperties = function (col) "";

//AString getImageSrc(in long row, in nsITreeColumn col);
PkgTreeModel.prototype.getImageSrc = function (row, col) null;

//long getLevel(in long index);
PkgTreeModel.prototype.getLevel = function (index) 0;

//long getParentIndex(in long rowIndex);
PkgTreeModel.prototype.getParentIndex = function (rowIndex) -1;

//long getProgressMode(in long row, in nsITreeColumn col);
// this method is only called for columns of type progressmeter

//AString getRowProperties(in long index);
PkgTreeModel.prototype.getRowProperties = function (index) "";


//boolean hasNextSibling(in long rowIndex, in long afterIndex);
// used to determine if the row at rowIndex has a nextSibling that occurs *after* the index specified by afterIndex
PkgTreeModel.prototype.hasNextSibling = function (rowIndex, afterIndex) false;

//boolean isContainer(in long index);
PkgTreeModel.prototype.isContainer = function (index) false;

//boolean isContainerEmpty(in long index);
PkgTreeModel.prototype.isContainerEmpty = function (index) true;

//boolean isContainerOpen(in long index);
PkgTreeModel.prototype.isContainerOpen = function (index) false;

//boolean isEditable(in long row, in nsITreeColumn col);
PkgTreeModel.prototype.isEditable = function (row, col) false;

//boolean isSelectable(in long row, in nsITreeColumn col);
// this method is only called if the selection type is cell or text

//boolean isSeparator(in long index);
PkgTreeModel.prototype.isSeparator = function (index) false;

//boolean isSorted();
PkgTreeModel.prototype.isSorted = function () false;

//void performAction(in wstring action);
PkgTreeModel.prototype.performAction = function (action) {};

//void performActionOnCell(in wstring action, in long row, in nsITreeColumn col);

//void performActionOnRow(in wstring action, in long row);

//void selectionChanged();
PkgTreeModel.prototype.selectionChanged = function () {
  conlog("proptree: selection changed");
};

//void setCellText(in long row, in nsITreeColumn col, in AString value);
// this method is called when the contents of the cell have been edited by the user
PkgTreeModel.prototype.setCellText = function (row, col, value) {};

//void setCellValue(in long row, in nsITreeColumn col, in AString value);
// this method is only called for columns of type other than text
PkgTreeModel.prototype.setCellValue = function (row, col, value) {};

//void setTree(in nsITreeBoxObject tree);
PkgTreeModel.prototype.setTree = function (treebox) {
  this.treebox = treebox;
};

//void toggleOpenState(in long index);
PkgTreeModel.prototype.toggleOpenState = function (index) {};


////////////////////////////////////////////////////////////////////////////////
exports.newWindow = function (win) {
  mainWin = win;
  treeModel = new PkgTreeModel();

  mainWin.addEventListener("load", function (e) {
    conlog("ONLOAD!");
    pkgTree = mainWin.document.getElementById("pkglist");
    pkgTree.view = treeModel;
    pkgTree.addEventListener("select", function (evt) { setButtonStatus(); });
    setButtonStatus();
  }, false);

  mainWin.addEventListener("unload", function (e) {
    conlog("UNLOAD!");
    removeSignalListener("package-downloader", onPagackeManagerSignal);
    removeSignalListener("package-manager", onPagackeManagerSignal);
    pkgList = null;
    pkgTree = null;
    treeModel = null;
    mainWin = null;
  }, false);

  addSignalListener("package-downloader", onPagackeManagerSignal);
  addSignalListener("package-manager", onPagackeManagerSignal);
  onPagackeManagerSignal(null);
};


exports.doUpdate = function () {
  let pi = getCurrentItem();
  if (pi && !pi.updating) {
    treeModel.setUpdatingForId(pi.pkgid, true);
    setButtonStatus();
    pkgman.update(pi.name);
  }
};


exports.doRemove = function () {
  let pi = getCurrentItem();
  if (!pi || pi.updating) return;
  let psvc = Cc["@mozilla.org/embedcomp/prompt-service;1"].getService(Ci.nsIPromptService);
  let ok = psvc.confirm(mainWin, "Guerilla Script deletion confirm", "Do you want to delete package '"+pi.name+"'?");
  //conlog(ok);
  if (ok) {
    pkgman.remove(pi.name);
    pkgList = null;
    mainWin.setTimeout(function () {
      treeModel.clear();
      onPagackeManagerSignal(null);
    }, 1);
  }
};


exports.doInfo = function () {
  let pi = getCurrentItem();
  if (!pi) return;
  let psvc = Cc["@mozilla.org/embedcomp/prompt-service;1"].getService(Ci.nsIPromptService);
  let args = {
    closeReason:"unknown",
    isPkgExists: function (name) {
      let nplist = pkgman.getPackageList();
      for (let pi of nplist.list) if (pi.name === name) return true;
      return false;
    },
    isUrlExists: function (url) {
      let nplist = pkgman.getPackageList();
      for (let pi of nplist.list) if (pi.url === url) return pi.name;
      return false;
    },
    name: pi.name,
    url: pi.url,
  };
  let dw = mainWin.openDialog("chrome://guerilla-script/content/guerilla-newpkg.xul", "_blank", "chrome,dialog,modal,centerscreen", args);
  dw.focus();
  if (args.closeReason !== "accept") return;
  conlog("name=[", args.name, "]");
  conlog("url=[", args.url, "]");
  //pkgman.install(args.name, args.url);
};


exports.doInstall = function () {
  let args = {
    closeReason:"unknown",
    isPkgExists: function (name) {
      let nplist = pkgman.getPackageList();
      for (let pi of nplist.list) if (pi.name === name) return true;
      return false;
    },
    isUrlExists: function (url) {
      let nplist = pkgman.getPackageList();
      for (let pi of nplist.list) if (pi.url === url) return pi.name;
      return false;
    },
  };
  let dw = mainWin.openDialog("chrome://guerilla-script/content/guerilla-newpkg.xul", "_blank", "chrome,dialog,modal,centerscreen", args);
  dw.focus();
  if (args.closeReason !== "accept") return;
  conlog("name=[", args.name, "]");
  conlog("url=[", args.url, "]");
  pkgman.install(args.name, args.url);
};
