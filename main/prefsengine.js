/* coded by Ketmar // Invisible Vector (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/txt/copying/ for more details.
 */
////////////////////////////////////////////////////////////////////////////////
// user code should define `PREFS_BRANCH`, `PREFS` and `PREFS_DIR_NAMES`


////////////////////////////////////////////////////////////////////////////////
function setDefaultPrefs () {
  let branch = Services.prefs.getDefaultBranch(PREFS_BRANCH);
  for (let [key, val] in Iterator(PREFS)) {
    switch (typeof(val)) {
      case "boolean": branch.setBoolPref(key, val); break;
      case "number": branch.setIntPref(key, val); break;
      case "string": branch.setCharPref(key, val); break;
    }
  }
}


////////////////////////////////////////////////////////////////////////////////
let knownDirs = {};

function getUserXXDir (name, createit) {
  let res = knownDirs[name];
  if (res) {
    // cached
    res = res.clone();
  } else {
    if (!(name in PREFS_DIR_NAMES)) throw new Error("unknown 'known' dir: '"+name+"'");
    //logError("getting dir '"+name+"'");
    // new, cache it
    let dn = "";
    try { dn = Services.prefs.getCharPref(PREFS_BRANCH+name); } catch (e) {}
    if (dn.length && dn[0] == '/') {
      res = Cc["@mozilla.org/file/local;1"].createInstance(Ci.nsILocalFile);
      res.initWithPath(dn);
    } else {
      const dirSvcProps = Cc["@mozilla.org/file/directory_service;1"].getService(Ci.nsIProperties);
      // get profile directory
      res = dirSvcProps.get("ProfD", Ci.nsIFile);
      if (dn.length) {
        for (let d of dn.split("/")) if (d.length) res.append(d);
      }
    }
    //logError("dir ["+name+"] = ["+res.path+"]");
    knownDirs[name] = res.clone();
  }
  if (createit && !res.exists()) res.create(res.DIRECTORY_TYPE, 0700);
  return res;
}

exportsGlobal.resetUserDirCache = function () { knownDirs = {}; };


// build dir getters
if ("PREFS_DIR_NAMES" in this) {
  let userdirs = {};
  for (let [iname, oname] in Iterator(PREFS_DIR_NAMES)) {
    let getter = tieto(this, getUserXXDir, iname, true);
    exportsGlobal["getUser"+oname] = getter;
    Object.defineProperty(userdirs, oname, {get: getter});
  }
  exportsGlobal.userdirs = userdirs;
}


////////////////////////////////////////////////////////////////////////////////
function getAddonPref (name) {
  if (typeof(name) == "string" && (name in PREFS)) {
    try {
      switch (typeof(PREFS[name])) {
        case "boolean": return Services.prefs.getBoolPref(PREFS_BRANCH+name);
        case "number": return Services.prefs.getIntPref(PREFS_BRANCH+name);
        case "string": return Services.prefs.getCharPref(PREFS_BRANCH+name);
      }
    } catch (e) {}
    return PREFS[name];
  }
  logError("k8extcarcass: invalid preference request for '"+name+"'");
  return null;
}


////////////////////////////////////////////////////////////////////////////////
function setAddonPref (name, val) {
  if (typeof(name) == "string" && (name in PREFS)) {
    try {
      switch (typeof(PREFS[name])) {
        case "boolean": val = !!val; Services.prefs.setBoolPref(PREFS_BRANCH+name, val); break;
        case "number": val = parseInt(val); Services.prefs.setIntPref(PREFS_BRANCH+name, val); break;
        case "string": val = ""+val; Services.prefs.setCharPref(PREFS_BRANCH+name, val); break;
      }
      return val;
    } catch (e) {}
  }
  logError("k8extcarcass: invalid preference set request for '"+name+"'");
  return null;
}


////////////////////////////////////////////////////////////////////////////////
let addonOptions = {
  _cache: {},
  _resetCache: function () { addonOptions._cache = {}; },
  _getOpt: function (name) {
    if (!(name in addonOptions._cache)) addonOptions._cache[name] = getAddonPref(name);
    //print("_getOpt: name='"+name+"'; val='"+addonOptions._cache[name]+"'");
    return addonOptions._cache[name];
  },
  _setOpt: function (name, val) {
    //print("_setOpt: name='", name, "'; val='", val, "'");
    addonOptions._cache[name] = val;
    return setAddonPref(name, val);
  },
  _setIntOpt: function (name, val) {
    if (typeof(val) !== "number") {
      let i = parseInt(""+val, 10);
      if (isNaN(i)) throw new Error("invalid integer for option '"+name+"': '"+val+"'");
      val = i;
    } else {
      val = Math.floor(val);
    }
    addonOptions._setOpt(name, val);
  },
  _setBoolOpt: function (name, val) { addonOptions._setOpt(name, !!val); },
  _setStrOpt: function (name, val) { addonOptions._setOpt(name, ""+val); },
};

// fill properties
(function () {
  for (let [n, v] in Iterator(PREFS)) {
    if (!n || typeof(n) !== "string") continue; // just in case
    if (n in PREFS_DIR_NAMES) continue; // this has special getters
    let getter = tieto(addonOptions, "_getOpt", n);
    let setter = false;
    switch (typeof(v)) {
      case "boolean": setter = tieto(addonOptions, "_setBoolOpt", n); break;
      case "number": setter = tieto(addonOptions, "_setIntOpt", n); break;
      case "string": setter = tieto(addonOptions, "_setStrOpt", n); break;
    }
    if (setter) Object.defineProperty(addonOptions, n, {get:getter, set:setter});
  }
})();
exportsGlobal.addonOptions = addonOptions;


////////////////////////////////////////////////////////////////////////////////
let prefObserver = {
  observe: function (aSubject, aTopic, aData) {
    //conlog("data: ["+aData+"]; subj="+aSubject+"; topic="+aTopic);
    //let name = aData.substr(PREFS_BRANCH.length);
    //conlog("name: ["+name+"]");
    //let prf = Services.prefs;
    //if (name == "openErrorConsoleOnStartup") addonOptions.openConsole = prf.getBoolPref(aData);
    if (aData.indexOf("dir") > 0) {
      resetUserDirCache();
    } else {
      addonOptions._resetCache();
    }
  },
};


////////////////////////////////////////////////////////////////////////////////
registerStartupHook("preferences", function () {
  setDefaultPrefs(); // always set the default prefs as they disappear on restart
  Services.prefs.addObserver(PREFS_BRANCH, prefObserver, false);
});


registerShutdownHook("preferences", function () {
  Services.prefs.removeObserver(PREFS_BRANCH, prefObserver);
});
