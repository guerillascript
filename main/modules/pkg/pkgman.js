/* coded by Ketmar // Invisible Vector (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/txt/copying/ for more details.
 */
////////////////////////////////////////////////////////////////////////////////
Components.utils.import("chrome://guerilla-script-jscode/content/modules/signals.jsm");
let asvc = Components.classes["@mozilla.org/alerts-service;1"].getService(Ci.nsIAlertsService);


addSignalListener("package-downloader", function (signame, state) {
  conlog("package-downloader: phase=", state.phase, "; url=", state.url);

  if (state.phase.substr(0, 6) === "error-") {
    // show notification
    asvc.showAlertNotification(
      "chrome://guerilla-script-misc/content/icon.png",
      "GuerillaScript package manager",
      "DOWNLOAD FAILED! ("+state.phase+")\n"+state.url,
      false
    );
    //error-main-file
    //error-main-content-type
    //error-main-metadata
    //error-resource-file
  } else if (state.phase === "complete") {
    // show notification
    asvc.showAlertNotification(
      "chrome://guerilla-script-misc/content/icon.png",
      "GuerillaScript package manager",
      "DOWNLOAD COMPLETE!\n"+state.url,
      false
    );
  }
});


////////////////////////////////////////////////////////////////////////////////
let dldr = null;
let lastUpdateCount = 0;
let pkgList = {lmod:-1};


////////////////////////////////////////////////////////////////////////////////
exports.getPackageList = function () {
  if (lastUpdateCount != pkgList.lmod) {
    pkgList.lmod = lastUpdateCount;
    pkgList.list = pkgDB.getActivePackages();
    //for (let pi of pkgList.list) conlog("id=", pi.id, "; name=", pi.name);
  }
  return pkgList;
};


exports.getLastUpdateTimeForId = function (pkgid) pkgDB.getPackageUpdateTimeById(pkgid);


////////////////////////////////////////////////////////////////////////////////
// queue object:
//  string name
//  string url
let queue = new Array();


////////////////////////////////////////////////////////////////////////////////
exports.__defineGetter__("inProgress", function () (dldr && dldr.running));


////////////////////////////////////////////////////////////////////////////////
exports.cancel = function () {
  if (dldr) {
    if (dldr.running) dldr.cancel();
    dldr = null;
  }
  queue = new Array();
};


////////////////////////////////////////////////////////////////////////////////
function pingQueue () {
  if (dldr) return; // in progress
  let qo; // queue object
  for (;;) {
    if (queue.length == 0) return; // nothing to do
    // get queue object
    qo = queue[0];
    queue.splice(0, 1); // remove first element
    if (!qo.url) {
      //conlog("NO URL for '", qo.name, "'");
      // no url: update request; find package
      let pi = pkgDB.getActivePackageByName(qo.name);
      if (pi) {
        /*
        conlog(
          "pi.id=", pi.id, "\n",
          "pi.name=", pi.name, "\n",
          "pi.dirname=", pi.dirname, "\n",
          "pi.version=", pi.version, "\n",
          "pi.url=", pi.url, "\n"
        );
        */
        qo.url = pi.url;
        break;
      }
      logError("can't update unknown package '"+qo.name+"'");
    } else {
      break;
    }
  }
  //conlog("GS: qo.name=", qo.name, "; qo.url=", qo.url);
  // new downloader
  dldr = new PkgDownloader(qo.url);
  let skipCancel = false;
  dldr.clearTempDir();
  dldr.onError = function () {
    /*++lastUpdateCount;*/
    emitSignal("package-manager", {name:qo.name, phase:"error"});
    logError("package '", qo.name, "': download failed!");
    dldr = null;
    pingQueue();
  };
  dldr.onCancel = function () {
    /*++lastUpdateCount;*/
    if (!skipCancel) {
      emitSignal("package-manager", {name:qo.name, phase:"cancelled"});
      logError("package '", qo.name, "': download cancelled!");
    }
    dldr = null;
    pingQueue();
  };
  dldr.onComplete = function () {
    dldr = null;
    let upd = pkgDB.checkAndUpdate(qo.name, this.pkg);
    conlog((upd ? "" : "NOT "), "UPDATED: '", qo.name, "'");
    if (upd) {
      ++lastUpdateCount;
      scacheAPI.reset();
    }
    emitSignal("package-manager", {name:qo.name, phase:"complete"});
    pingQueue();
  };
  dldr.onMainReceived = function () {
    conlog("package '", qo.name, "': received main file");
    let res = pkgDB.checkAndUpdate(qo.name, this.pkg, {checkOnly:true});
    if (!res) {
      conlog("package '", qo.name, "' is up-to-date");
      skipCancel = true;
      emitSignal("package-manager", {name:qo.name, phase:"complete"});
    }
    return res;
  };
  emitSignal("package-manager", {name:qo.name, phase:"started"});
  dldr.start();
}


////////////////////////////////////////////////////////////////////////////////
exports.install = function (name, url) {
  if (typeof(name) !== "string" || !name) throw new Error("invalid package name");
  if (typeof(url) !== "string" || !(/https?:/.test(url))) throw new Error("invalid package url");
  // queue
  let found = false;
  for (let qo of queue) {
    if (qo.name == name) {
      if (qo.url != url) throw new Error("conflicting URLs for package '"+name+"'");
      found = true;
      break;
    }
  }
  if (!found) queue.push({name:name, url:url});
  pingQueue();
}


////////////////////////////////////////////////////////////////////////////////
exports.update = function (name) {
  if (typeof(name) !== "string" || !name) throw new Error("invalid package name");
  // queue
  let found = false;
  for (let qo of queue) {
    if (qo.name == name) {
      found = true;
      break;
    }
  }
  if (!found) queue.push({name:name, url:null});
  pingQueue();
}


////////////////////////////////////////////////////////////////////////////////
exports.remove = function (name) {
  if (typeof(name) !== "string" || !name) throw new Error("invalid package name");
  if (dldr && dldr.running) throw new Error("package manager is busy");
  try {
    pkgDB.removePackage(name);
    conlog("package '", name, "' removed");
  } catch (e) { logException("WTF", e); }
  ++lastUpdateCount;
  scacheAPI.reset();
}
