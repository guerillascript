/* coded by Ketmar // Invisible Vector (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/txt/copying/ for more details.
 */
////////////////////////////////////////////////////////////////////////////////
function nowSeconds () Math.floor(Date.now()/1000);


////////////////////////////////////////////////////////////////////////////////
function PackageDB () {
  this.dbObj = null;
  return this;
}


PackageDB.prototype.STATE_OK = 0;
PackageDB.prototype.STATE_INACTIVE = 1;
PackageDB.prototype.STATE_DISABLED = 2; // due to some errors


PackageDB.prototype.__defineGetter__("db", function () {
  if (this.dbObj == null) {
    if (!this.dbFile) {
      this.dbFile = getUserPkgDBDir();
      //conlog("** package db dir: [", this.dbFile.path, "]");
      //this.dbFile.append("database");
      if (!this.dbFile.exists()) this.dbFile.create(this.dbFile.DIRECTORY_TYPE, 0700);
      this.dbFile.append("packages.db");
      //conlog("package db: [", this.dbFile.path, "]");
    }
    this.dbObj = Services.storage.openDatabase(this.dbFile);
    // the auto_vacuum pragma has to be set before the table is created
    this.dbObj.executeSimpleSQL("PRAGMA auto_vacuum = NONE;");
    this.dbObj.executeSimpleSQL("PRAGMA journal_mode = DELETE;");
    //
    this.dbObj.executeSimpleSQL(
      "CREATE TABLE IF NOT EXISTS packages (\n"+
      "  id INTEGER PRIMARY KEY AUTOINCREMENT,\n"+ /* unique package id */
      "  name TEXT NOT NULL UNIQUE,\n"+ /* package name */
      "  dirname TEXT NOT NULL UNIQUE,\n"+ /* package dir name, valid disk file name inside package dir */
      "  version TEXT NOT NULL,\n"+ /* package version */
      "  downurl TEXT NOT NULL,\n"+ /* package update url */
      "  state INTEGER NOT NULL DEFAULT 0\n"+ /* see PackageDB.STATE_* */
      ");"
    );
    this.dbObj.executeSimpleSQL("CREATE INDEX IF NOT EXISTS packages_by_name ON packages (name);");
    this.dbObj.executeSimpleSQL("CREATE INDEX IF NOT EXISTS packages_by_name_and_state ON packages (name, state);");
    // i don't want to alter `packages` table, so...
    this.dbObj.executeSimpleSQL(
      "CREATE TABLE IF NOT EXISTS packagetimes (\n"+
      "  pkgid INTEGER NOT NULL,\n"+ /* unique package id */
      "  lastuptime INTEGER NOT NULL DEFAULT 0\n"+ /* last successfull update unixtime, in seconds */
      ");"
    );
    this.dbObj.executeSimpleSQL("CREATE INDEX IF NOT EXISTS packageups_by_pkgid ON packagetimes (pkgid);");
    //
    this.dbObj.executeSimpleSQL(
      "CREATE TABLE IF NOT EXISTS resources (\n"+
      "  id INTEGER PRIMARY KEY AUTOINCREMENT,\n"+ /* unique file id */
      "  pkgid INTEGER NOT NULL,\n"+ /* unique package id */
      "  url TEXT NOT NULL UNIQUE,\n"+ /* file url */
      "  diskname TEXT NOT NULL,\n"+ /* disk file name; ""' means "main package file" */
      "  sha512 TEXT NOT NULL,\n"+ /* sha-512 of the disk file */
      "  resname TEXT NOT NULL DEFAULT '',\n"+ /* resource name for named resources */
      "  contenttype TEXT NOT NULL DEFAULT 'application/octet-stream'\n"+ /* resource content type */
      ");"
    );
    this.dbObj.executeSimpleSQL("CREATE INDEX IF NOT EXISTS rsrcs_by_pkgid ON resources (pkgid);");
    this.dbObj.executeSimpleSQL("CREATE INDEX IF NOT EXISTS rsrcs_by_pkgid_and_name ON resources (pkgid, resname);");
    //
    this.dbObj.executeSimpleSQL(
      "CREATE TABLE IF NOT EXISTS requires (\n"+
      "  id INTEGER PRIMARY KEY AUTOINCREMENT,\n"+ /* unique file id; first one is main js file */
      "  pkgid INTEGER NOT NULL,\n"+ /* unique package id */
      "  url TEXT NOT NULL UNIQUE,\n"+ /* file url */
      "  diskname TEXT NOT NULL,\n"+ /* disk file name */
      "  sha512 TEXT NOT NULL\n"+ /* sha-512 of the disk file */
      ");"
    );
    this.dbObj.executeSimpleSQL("CREATE INDEX IF NOT EXISTS reqs_by_pkgid ON requires (pkgid);");
    this.dbObj.executeSimpleSQL("CREATE INDEX IF NOT EXISTS reqs_by_pkgid_and_id ON requires (pkgid, id);");
    // run vacuum once manually to switch to the correct auto_vacuum mode for
    // databases that were created with incorrect auto_vacuum
    this.dbObj.executeSimpleSQL("VACUUM;");
  }
  return this.dbObj;
});


PackageDB.prototype.__defineGetter__("opened", function () {
  return (this.dbObj != null);
});


PackageDB.prototype.close = function () {
  if (this.dbObj) {
    this.dbObj.close();
    this.dbObj = null;
    this.dbFile = null;
  }
};


PackageDB.prototype.getActivePackageByName = function (name) {
  if (typeof(name) !== "string" || !name) return null;
  let stmt = this.db.createStatement("SELECT id,name,dirname,version,downurl FROM packages WHERE state=0 AND name=:name LIMIT 1");
  try {
    stmt.params.name = name;
    if (stmt.executeStep()) {
      let res = {
        id: stmt.row.id,
        name: stmt.row.name,
        dirname: stmt.row.dirname,
        version: stmt.row.version,
        url: stmt.row.downurl,
        toString: function () "PkgInfo<id:"+this.id+";name:'"+this.name+"';version:'"+this.version+"';url:'"+this.url+"'>",
      };
      stmt.reset();
      return res;
    }
  } catch (e) {}
  stmt.reset();
  return null;
};


// -1: unknown
PackageDB.prototype.getPackageUpdateTimeById = function (pkgid) {
  //conlog("*** pkgid=", pkgid);
  if (typeof(pkgid) !== "number") return -1;
  let stmt = this.db.createStatement("SELECT lastuptime FROM packagetimes WHERE pkgid=:pkgid LIMIT 1");
  try {
    stmt.params.pkgid = pkgid;
    if (stmt.executeStep()) {
      let res = stmt.row.lastuptime;
      //conlog("pkgid=", pkgid, "; lastuptime=", res);
      if (isNaN(res)) res = -1;
      stmt.reset();
      return res;
    }
    //conlog("pkgid=", pkgid, "; alas");
  } catch (e) {
    logException("getPackageUpdateTimeById", e);
  }
  stmt.reset();
  return -1;
};


// -1: unknown
PackageDB.prototype.getPackageUpdateTimeByName = function (name) {
  let nfo = this.getActivePackageByName(name);
  if (!nfo) return -1;
  return this.getPackageUpdateTimeById(nfo.id);
};


PackageDB.prototype.setPackageUpdateTimeById = function (pkgid, luptime) {
  if (typeof(pkgid) !== "number") return false;
  if (typeof(luptime) !== "number") return false;
  let stmt = this.db.createStatement("UPDATE packagetimes SET lastuptime=:luptime WHERE pkgid=:pid");
  try {
    stmt.params.pkgid = pkgid;
    stmt.params.luptime = luptime;
    stmt.execute();
  } catch (e) {}
  stmt.reset();
  return true;
};


PackageDB.prototype.getActivePackages = function () {
  let res = new Array();
  let stmt = this.db.createStatement("SELECT id,name,dirname,version,downurl FROM packages WHERE state=0 ORDER BY name ASC");
  try {
    while (stmt.executeStep()) {
      let rr = {
        id: stmt.row.id,
        name: stmt.row.name,
        dirname: stmt.row.dirname,
        version: stmt.row.version,
        url: stmt.row.downurl,
        toString: function () "PkgInfo<id:"+this.id+";name:'"+this.name+"';version:'"+this.version+"';url:'"+this.url+"'>",
      };
      res.push(rr);
    }
  } catch (e) {
  } finally {
    stmt.reset();
  }
  return res;
};


PackageDB.prototype.getPackageJSFiles = function (pkgid) {
  let res = new Array();
  let stmt = this.db.createStatement("SELECT diskname FROM requires WHERE pkgid=:pkgid ORDER BY id ASC");
  try {
    stmt.params.pkgid = pkgid;
    while (stmt.executeStep()) {
      res.push(stmt.row.diskname);
    }
  } catch (e) {
  } finally {
    stmt.reset();
  }
  return res;
};


////////////////////////////////////////////////////////////////////////////////
// normalize string, so it can be used as disk file name
function normDiskStr (s) {
  const validRE = /^[-A-Za-z_0-9.]$/;
  let res = "";
  for (let ch of s) {
    if (!validRE.test(ch)) ch = "_";
    res += ch;
  }
  if (res.length && res[0] == "_") res = "pkg"+res;
  return res;
}


// options:
//   bool checkOnly
//   bool ignoreVersion
//   bool forceUpdate
// returns true if package should be updated
PackageDB.prototype.checkAndUpdate = function (pkgname, dnpkg, options) {
  if (typeof(pkgname) !== "string" || !pkgname) throw new Error("invalid package name");
  if (typeof(dnpkg) !== "object") throw new Error("package info object expected");
  if (typeof(options) !== "object") options = {};

  let pdb = this.db;

  function getPkgInfo () {
    let stmt = pdb.createStatement("SELECT id,version,dirname FROM packages WHERE name=:name LIMIT 1");
    stmt.params.name = pkgname;
    if (stmt.step()) return {id:stmt.row.id, version:stmt.row.version, dirname:stmt.row.dirname};
    return null;
  }

  let pi = getPkgInfo();
  if (pi && !options.forceUpdate && !options.ignoreVersion) {
    if (pi.version == dnpkg.version) {
      // no need to do anything
      if (!options.checkOnly) {
        // update update time ;-)
        this.setPackageUpdateTimeById(pi.id, nowSeconds());
      }
      return false;
    }
  }

  // need to update
  if (options.checkOnly) return true;

  try {
    pdb.executeSimpleSQL("BEGIN TRANSACTION;");

    // build package directory, and create it if necessary
    let pkdir = getUserPkgDir();
    if (!pi) {
      // we want new directory
      pi = {};
      pi.version = ""+dnpkg.version;
      pi.dirname = normDiskStr(pkgname);
      pkdir.append(pi.dirname);
      pkdir.createUnique(pkdir.DIRECTORY_TYPE, 0700);
      pi.dirname = pkdir.leafName;
      // create database record, so we can get package id from it
      {
        let stmt = pdb.createStatement("INSERT OR REPLACE INTO packages (name, dirname, version, downurl, state) VALUES (:name, :dirname, :version, :downurl, 0)");
        try {
          stmt.params.name = pkgname;
          stmt.params.dirname = pi.dirname;
          stmt.params.version = pi.version;
          stmt.params.downurl = dnpkg.downurl;
          stmt.execute();
        } finally {
          stmt.reset();
        }
      }
      pi = getPkgInfo();
      if (!pi) throw new Error("something is wrong with package database");
    } else {
      // we want existing directory
      pkdir.append(pi.dirname);
      if (pkdir.exists() && !pkdir.isDirectory()) throw new Error("something is wrong with package storage");
    }

    // clear package directory
    try { pkdir.remove(true); } catch (e) {}
    try { if (!pkdir.exists()) pkdir.create(pkdir.DIRECTORY_TYPE, 0700); } catch (e) {}

    function copyFile (srcpath) {
      let fl = Cc["@mozilla.org/file/local;1"].createInstance(Ci.nsILocalFile);
      fl.initWithPath(srcpath);
      //conlog("copying ["+srcpath+"] to ["+pkdir.path+"/]");
      fl.copyTo(pkdir, fl.leafName);
      let nn = pkdir.clone();
      nn.append(fl.leafName);
      return nn.path;
    }

    //TODO: make this async?
    // copy package files
    dnpkg.diskname = copyFile(dnpkg.diskname);
    for (let res of dnpkg.resources) res.diskname = copyFile(res.diskname);

    // update resources
    // ah, id is always integer here
    pdb.executeSimpleSQL("DELETE FROM resources WHERE pkgid="+pi.id);
    pdb.executeSimpleSQL("DELETE FROM requires WHERE pkgid="+pi.id);

    // fill requires
    {
      let stmt = pdb.createStatement("INSERT OR REPLACE INTO requires (pkgid, url, diskname, sha512) VALUES (:pkgid, :url, :diskname, :sha512)");
      // insert main js
      stmt.params.pkgid = pi.id;
      stmt.params.url = dnpkg.downurl;
      stmt.params.diskname = dnpkg.diskname;
      stmt.params.sha512 = dnpkg.sha512;
      stmt.execute();
      stmt.reset();
      // insert other js files
      for (let res of dnpkg.resources) {
        if (res.resname !== null) continue;
        stmt.reset();
        stmt.params.pkgid = pi.id;
        stmt.params.url = res.url;
        stmt.params.diskname = res.diskname;
        stmt.params.sha512 = res.sha512;
        stmt.execute();
        stmt.reset();
      }
    }

    // fill resources
    {
      let stmt = pdb.createStatement("INSERT OR REPLACE INTO resources (pkgid, url, diskname, sha512, resname, contenttype) VALUES (:pkgid, :url, :diskname, :sha512, :resname, :ct)");
      for (let res of dnpkg.resources) {
        if (res.resname === null) continue;
        stmt.reset();
        stmt.params.pkgid = pi.id;
        stmt.params.url = res.url;
        stmt.params.diskname = res.diskname;
        stmt.params.sha512 = res.sha512;
        stmt.params.resname = res.resname;
        stmt.params.ct = res.contenttype||"application/octet-stream";
        stmt.execute();
        stmt.reset();
      }
    }

    pdb.executeSimpleSQL("COMMIT;");
    // update update time ;-)
    this.setPackageUpdateTimeById(pi.id, nowSeconds());
    return true;
  } catch (e) {
    pdb.executeSimpleSQL("ROLLBACK;");
    throw e;
  }
};


////////////////////////////////////////////////////////////////////////////////
// this returns array of xnfos:
//  string path: path to main script
//  string[] reqs: list of required files
//  {string name} rsrc: resources dict, keyed by resource name, value is string path
PackageDB.prototype.getActivePackagesForCache = function () {
  let res = new Array();
  let ids = [];

  let pdb = this.db;

  function getPackageIds () {
    let stmt = pdb.createStatement("SELECT id FROM packages WHERE state=0 ORDER BY name ASC");
    try {
      while (stmt.executeStep()) ids.push(id);
    } catch (e) {
    } finally {
      stmt.reset();
    }
  }

  getPackageIds();
  //conlog("packages: "+ids.length);

  let reqmt = pdb.createStatement("SELECT diskname FROM requires WHERE pkgid=:pkgid ORDER BY id ASC");
  let resmt = pdb.createStatement("SELECT diskname,resname,contenttype FROM resources WHERE pkgid=:pkgid");
  for (let piid of ids) {
    // first JS files
    reqmt.reset();
    reqmt.params.pkgid = piid;

    let pi = {id:piid, reqs:null};

    while (reqmt.executeStep()) {
      //conlog("+++ req: ", reqmt.row.diskname);
      if (!pi.reqs) {
        pi.path = reqmt.row.diskname;
        pi.reqs = new Array();
      } else {
        pi.reqs.push(reqmt.row.diskname);
      }
    }
    if (!pi.reqs) continue; //wtf?!

    // then resources
    resmt.reset();
    resmt.params.pkgid = piid;
    pi.rsrc = {};
    while (resmt.executeStep()) {
      //conlog("res: ", resmt.row.resname, " : ", resmt.row.diskname);
      pi.rsrc[resmt.row.resname] = {path:resmt.row.diskname, contenttype:resmt.row.contenttype};
    }
    res.push(pi);
  }
  reqmt.reset();
  resmt.reset();

  return res;
};


////////////////////////////////////////////////////////////////////////////////
PackageDB.prototype.removePackage = function (name) {
  if (typeof(name) !== "string" || !name) throw new Error("invalid package name");
  let pi = this.getActivePackageByName(name);
  if (!pi) throw new Error("unknown package '"+name+"'");
  let pdb = this.db;
  try {
    pdb.executeSimpleSQL("BEGIN TRANSACTION;");
    pdb.executeSimpleSQL("DELETE FROM packages WHERE id="+pi.id);
    pdb.executeSimpleSQL("DELETE FROM packagetimes WHERE pkgid="+pi.id);
    pdb.executeSimpleSQL("DELETE FROM resources WHERE pkgid="+pi.id);
    pdb.executeSimpleSQL("DELETE FROM requires WHERE pkgid="+pi.id);
    pdb.executeSimpleSQL("COMMIT;");
    // clear package directory
  } catch (e) {
    pdb.executeSimpleSQL("ROLLBACK;");
    throw e;
  }
  let pkdir = getUserPkgDir();
  pkdir.append(pi.dirname);
  if (pkdir.exists() && pkdir.isDirectory()) {
    try { pkdir.remove(true); } catch (e) {}
  }
}


////////////////////////////////////////////////////////////////////////////////
exports.PackageDB = PackageDB;
