/* coded by Ketmar // Invisible Vector (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/txt/copying/ for more details.
 */
////////////////////////////////////////////////////////////////////////////////
require("utils/utils");
let {parseMeta} = require("utils/metaparser");

////////////////////////////////////////////////////////////////////////////////
Components.utils.import("chrome://guerilla-script-jscode/content/modules/signals.jsm");


////////////////////////////////////////////////////////////////////////////////
function genFileObj (fname) {
  if (typeof(fname) !== "string") throw new Error("non-string file name in `genFileObj()`");
  if (fname.length == 0) throw new Error("empty file name in `genFileObj()`");
  if (fname[0] == '/') {
    let fl = Cc["@mozilla.org/file/local;1"].createInstance(Ci.nsILocalFile);
    fl.initWithPath(fname);
    return fl;
  } else {
    let fl = getUserPkgTempDir();
    for (let p of fname.split("/")) if (p && p != ".") fl.append(p);
    return fl;
  }
}


////////////////////////////////////////////////////////////////////////////////
function calcFileSha512 (fname) {
  let fl = Cc["@mozilla.org/file/local;1"].createInstance(Ci.nsILocalFile);
  fl.initWithPath(fname);
  let istream = Cc["@mozilla.org/network/file-input-stream;1"].createInstance(Ci.nsIFileInputStream);
  // open for reading
  istream.init(fl, 0x01, 0400, istream.CLOSE_ON_EOF); // 0x01: PR_RDONLY
  let chan = Cc["@mozilla.org/security/hash;1"].createInstance(Ci.nsICryptoHash);
  chan.init(chan.SHA512);
  chan.updateFromStream(istream, 0xffffffff);
  // pass false here to get binary data back
  let hash = chan.finish(false);
  //return [("0"+hash.charCodeAt(i).toString(16)).slice(-2) for (i in hash)].join("");
  var res = "";
  for (var i = 0; i < hash.length; ++i) res += ("0"+hash.charCodeAt(i).toString(16)).slice(-2);
  return res;
}


////////////////////////////////////////////////////////////////////////////////
// package downloader
// this downloads all package files and creates package object:
//   string downurl
//   string name
//   string version
//   string diskname
//   string sha512
//   variant[] meta
//   ResInfo[] resources
// ResInfo:
//   string url
//   string diskname
//   string resname
//   string sha512


// state:
// -1: cancelled
//  0: not started
//  1: running
//  2: aborted with error
//  3: complete
function PkgDownloader (aurl) {
  this.mainURL = aurl;
  this.state = 0;
  this.reslist = []; // resources to download: {name, url}; if `name` is null, this is js file
  this.curdown = null;
  //this.curres = null;
  //this.pkg = {};
  this.onError = function () {};
  this.onComplete = function () {};
  this.onCancel = function () {};
  // return `false` from `onMainReceived` to abort downloading and call `onCancel`
  this.onMainReceived = function () { return true; };
  return this;
}


PkgDownloader.prototype.clearTempDir = function () {
  let fl = getUserPkgTempDir();
  try { fl.remove(true); } catch (e) {}
  try { if (!fl.exists()) fl.create(fl.DIRECTORY_TYPE, 0700); } catch (e) {}
};


PkgDownloader.prototype.__defineGetter__("started", function () { return (this.state > 0); }); // was started, not necessarily running
PkgDownloader.prototype.__defineGetter__("running", function () { return (this.state == 1); });
PkgDownloader.prototype.__defineGetter__("cancelled", function () { return (this.state < 0); });
PkgDownloader.prototype.__defineGetter__("aborted", function () { return (this.state == 2); });
PkgDownloader.prototype.__defineGetter__("complete", function () { return (this.state == 3); });


PkgDownloader.prototype.genFileName = function (ext) {
  let fl = getUserPkgTempDir();
  if (typeof(ext) === "undefined" || !ext) {
    fl.append("pkgmain.js");
  } else {
    if (ext[0] != ".") ext = "."+ext;
    fl.append("pkgres"+ext);
  }
  fl.createUnique(fl.NORMAL_FILE_TYPE, 0600);
  let res = fl.path;
  //conlog("temp file: [", res, "]");
  //fl.close();
  return res;
}


// https://raw.githubusercontent.com/adsbypasser/adsbypasser/v5.31.0/css/align_center.css
PkgDownloader.prototype.getExtFromUrl = function (url) {
  let ioSvc = Cc["@mozilla.org/network/io-service;1"].getService(Ci.nsIIOService);

  function uriFromUrl (url, base) {
    let baseUri = null;
    if (typeof(base) === "string") {
      baseUri = uriFromUrl(base);
    } else if (base) {
      baseUri = base;
    }
    try {
      return ioSvc.newURI(url, null, baseUri);
    } catch (e) {
      return null;
    }
  }

  try {
    let uo = uriFromUrl(url);
    let path = uo.path;
    if (!path || path[path.length-1] == "/") return null;
    let mt = path.match(/(\.[^.]+)$/);
    if (mt) return mt[1];
  } catch (e) {}
  return null;
}


// callback:
//   nsIHttpChannel chan
//   bool success
//   string filename
//   string url
//   bool cancelled
PkgDownloader.prototype.downloadFile = function (destfname, aurl, callback) {
  conlog("downloading: ", aurl);
  emitSignal("package-downloader", {url:aurl, phase:"preparing-url"});
  let ioSrv = Cc["@mozilla.org/network/io-service;1"].getService(Ci.nsIIOService)
  let done = false, cancelled = false;
  let fl = genFileObj(destfname);
  let downObserver = {
    onDownloadComplete: function (dldr, request, ctxt, status, fl) {
      emitSignal("package-downloader", {url:aurl, phase:(status == 0 ? "done-url" : "error-url")});
      done = true;
      let cc = request.QueryInterface(Ci.nsIHttpChannel);
      if (!cancelled && callback) {
        try {
          callback(cc, (status == 0 && !cancelled), destfname, aurl, cancelled);
        } catch (e) {
          logException("downloadFile error", e);
        }
      }
    },
  };
  let dldr = Cc["@mozilla.org/network/downloader;1"].createInstance(Ci.nsIDownloader);
  dldr.init(downObserver, fl);
  let httpChan = ioSrv.newChannel(aurl, "", null).QueryInterface(Ci.nsIHttpChannel);
  httpChan.setRequestHeader("User-Agent", "GuerillaScript/0.666", false);
  httpChan.redirectionLimit = 16;
  httpChan.asyncOpen(dldr, fl);
  emitSignal("package-downloader", {url:aurl, phase:"started-url"});
  //conlog("downloading...");
  return {
    get done () done,
    get filename () destfname,
    get url () aurl,
    cancel: function () {
      if (!done && !cancelled) {
        cancelled = true;
        httpChan.cancel(Cr.NS_BINDING_ABORTED);
      }
    },
  }
};


PkgDownloader.prototype.cancel = function () {
  if (this.state != 1) throw new Error("can't cancel from invalid state");
  if (this.state == 1 && this.curdown) {
    this.curdown.cancel();
    this.curdown = null;
  }
  this.state = -1;
  if ("pkg" in this) delete this.pkg;
  if ("curres" in this) delete this.pkg;
  this.onCancel();
};


PkgDownloader.prototype.start = function () {
  if (this.state != 0) throw new Error("can't start from invalid state");
  this.state = 1;
  let fname = this.genFileName();
  //conlog("fname: [", fname, "]");
  try {
    this.curdown = this.downloadFile(fname, this.mainURL, tieto(this, "downCBMain"));
  } catch (e) {
    try { let fl = genFileObj(fname); fl.remove(false); } catch (e) {}
    this.state = 2;
    this.onError();
  }
};


PkgDownloader.prototype.__abort = function (skipOnError) {
  this.state = 2;
  if ("pkg" in this) delete this.pkg;
  if ("curres" in this) delete this.pkg;
  if (!skipOnError) try { this.onError(); } catch (e) { logException("onError", e); }
};


PkgDownloader.prototype._abort = function (filename, cancelled) {
  if (typeof(filename) === "string") {
    try { let fl = genFileObj(fname); fl.remove(false); } catch (e) {}
  }
  this.__abort(cancelled);
};


PkgDownloader.prototype.downCBMain = function (chan, success, filename, url, cancelled) {
  //conlog("downCBMain: [", filename, "] : ", success, " : ", chan.responseStatus, " : ", chan.requestSucceeded);
  this.curdown = null;
  try {
    if (!success || !chan.requestSucceeded) {
      // failed to download main file
      emitSignal("package-downloader", {url:this.mainURL, phase:"error-main-file"});
      this._abort(filename, cancelled);
      return;
    }
    // main file downloaded
    let ct = (chan.getResponseHeader("Content-Type")||"");
    if (!(/^text\/javascript(?:;|$)/.test(ct)) && !(/^text\/plain(?:;|$)/.test(ct))) {
      // invalid content type
      emitSignal("package-downloader", {url:this.mainURL, phase:"error-main-content-type"});
      this._abort(filename, cancelled);
      return;
    }
    // parse metadata
    let meta = parseMeta(fileReadText(genFileObj(filename)));
    if (meta.length == 0) {
      emitSignal("package-downloader", {url:this.mainURL, phase:"error-main-metadata"});
      this._abort(filename, cancelled);
      return;
    }
    this.pkg = {};
    this.pkg.downurl = this.mainURL;
    this.pkg.meta = meta;
    // get package name
    let name = meta.getField("name");
    if (name) {
      this.pkg.name = name.value;
    } else {
      name = diskname.split("/");
      this.pkg.name = name[name.length-1];
    }
    // get package version
    let ver = meta.getField("version");
    this.pkg.version = (ver ? ver.value : "");
    // diskname and sum
    this.pkg.diskname = filename;
    this.pkg.sha512 = calcFileSha512(this.pkg.diskname);
    // now build download list
    this.pkg.resources = new Array();
    let resList = new Array(); // resources to download: {name, url}
    // js files
    let rqf = {};
    meta.forEachField("require", function (kv) {
      if (!rqf[kv.value]) {
        if (/^https?:/.test(kv.value)) resList.push({name:null, url:kv.value});
        rqf[kv.value] = true;
      }
    });
    // resources
    rqf = {};
    meta.forEachField("resource", function (kv) {
      let rn = kv.value.name;
      let ru = kv.value.url;
      if (!rqf[rn] && (/^https?:/.test(ru))) rqf[rn] = ru;
    });
    for (let [rn, ru] in Iterator(rqf)) resList.push({name:rn, url:ru});
    this.reslist = resList;

    if (!this.onMainReceived()) {
      // cancel downloading
      this.onCancel();
      return;
    }

    this.downloadNextResource();
  } catch (e) {
    logException("DOWN", e);
    this.__abort(false);
  }
};


PkgDownloader.prototype.downloadNextResource = function () {
  this.curdown = null;
  if (this.reslist.length == 0) {
    conlog("package download complete");
    emitSignal("package-downloader", {url:this.mainURL, phase:"complete"});
    this.state = 3;
    try { this.onComplete(); } catch (e) { logException("DOWN", e); }
    return;
  }
  // we have some resource to download, do it
  try {
    let res = this.reslist[0];
    conlog("downloading resource '", res.name, "' from: ", res.url);
    let ext = this.getExtFromUrl(res.url);
    if (!ext) {
      emitSignal("package-downloader", {url:this.mainURL, phase:"error-resource-file"});
      this._abort(filename, cancelled);
      return;
    }
    this.curres = res;
    this.reslist = this.reslist.slice(1);
    let fname = this.genFileName(ext);
    this.curdown = this.downloadFile(fname, res.url, tieto(this, "downCBNextRes"));
  } catch (e) {
    logException("DOWN", e);
    // just in case
    this.__abort(false);
  }
};


PkgDownloader.prototype.downCBNextRes = function (chan, success, filename, url, cancelled) {
  this.curdown = null;
  if (!success || !chan.requestSucceeded) { this._abort(filename, cancelled); return; } // failed to download resource file
  try {
    let ct = (chan.getResponseHeader("Content-Type")||"application/octet-stream");
    let resobj = {};
    resobj.url = url;
    resobj.diskname = filename;
    resobj.resname = this.curres.name; // string or null
    resobj.sha512 = calcFileSha512(resobj.diskname);
    resobj.contenttype = ct;
    this.pkg.resources.push(resobj);
    this.downloadNextResource();
  } catch (e) {
    logException("DOWN", e);
    // just in case
    this.__abort(false);
  }
};


////////////////////////////////////////////////////////////////////////////////
exports.PkgDownloader = PkgDownloader;
