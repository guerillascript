/*
 * Copyright 2015 Ketmar Dark <ketmar@ketmar.no-ip.org>
 * Portions copyright 2004-2007 Aaron Boodman
 * Contributors: See contributors list in install.rdf and CREDITS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * Note that this license applies only to the Greasemonkey extension source
 * files, not to the user scripts which it runs. User scripts are licensed
 * separately by their authors.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 */
////////////////////////////////////////////////////////////////////////////////
uses("resource://gre/modules/XPCOMUtils.jsm");

let {closeStorageObjects, createSandbox, runInSandbox} = require("sbapi/sandbox");

let stripCredsRE = new RegExp("(://)([^:/]+)(:[^@/]+)?@");

//const conService = Cc["@mozilla.org/consoleservice;1"].getService(Ci.nsIConsoleService);
//function logStr (str) conService.logStringMessage(str);

////////////////////////////////////////////////////////////////////////////////
function runScripts (theWin, docStart) {
  //logStr("000");
  if (!addonOptions.active) {
    //logStr("001");
    return; // we are inactive
  }
  //logStr("002");
  if (!theWin) return;

  //logStr("003");
  let theDoc = theWin.document;
  if (!theDoc) return; //document not provided, it is undefined likely
  if (!theDoc instanceof Ci.nsIDOMHTMLDocument) return; //not html document, so its likely an xul document


  //logStr("004");
  // When content does (e.g.) history.replacestate() in an inline script,
  // the location.href changes between document-start and document-end time.
  // But the content can call replacestate() much later, too.  The only way to
  // be consistent is to ignore it.  Luckily, the  document.documentURI does
  // _not_ change, so always use it when deciding whether to run scripts.
  var url = theDoc.documentURI||"";
  if (!scacheAPI.isGoodScheme(url)) return;

  // ignore user/pass in the URL
  url = url.replace(stripCredsRE, "$1");

  let sclist = scacheAPI.scriptsForUrl(/*theDoc.location.href*/url, (theWin.frameElement ? true : false), (docStart ? "document-start" : "document-end"));
  if (addonOptions.debugMode && sclist !== false) debuglog("document-"+(docStart ? "start" : "end")+" for "+url+" ("+(sclist ? sclist.length : 0)+" scripts)");
  if (!sclist || sclist.length == 0) return;
  if (addonOptions.debugMode) {
    let s = "=== "+sclist.length+" scripts found for ["+url+"] ===";
    for (let f = 0; f < sclist.length; ++f) s += "\n  #"+f+": '"+sclist[f].name+"'";
    debuglog(s);
  }

  // each script is independent (expect unwrapped), so create separate sandboxes
  let uwpsbox = false;
  for (let nfo of sclist) {
    if (nfo.unwrapped) {
      // unwrapped script
      if (addonOptions.debugMode) {
        debuglog("["+url+"]: unwrapped script '"+nfo.name+"'");
        //scacheAPI.dumpScriptNfo(nfo);
      }
      if (!uwpsbox) uwpsbox = createSandbox(theWin, nfo, url);
      runInSandbox(uwpsbox, nfo);
    } else {
      // wrapped script
      if (addonOptions.debugMode) {
        debuglog("["+url+"]: wrapped script '"+nfo.name+"'");
        //scacheAPI.dumpScriptNfo(nfo);
      }
      let sbox = createSandbox(theWin, nfo, url);
      runInSandbox(sbox, nfo);
    }
  }
}


////////////////////////////////////////////////////////////////////////////////
function ContentObserver () {}


ContentObserver.prototype.QueryInterface = XPCOMUtils.generateQI([Ci.nsIObserver]);


ContentObserver.prototype.contentLoad = function (evt) {
  var contentWin = evt.target.defaultView;
  // now that we've seen any first load event, stop listening for any more
  contentWin.removeEventListener("DOMContentLoaded", gContentLoad, true);
  contentWin.removeEventListener("load", gContentLoad, true);
  //debuglog("document-end for "+contentWin.document.documentURI);
  runScripts(contentWin, false);
};


ContentObserver.prototype.observe = function (aSubject, aTopic, aData) {
  if (!addonOptions.active) return; // we are inactive
  if (aTopic == "document-element-inserted") {
    let doc = aSubject;
    let win = doc && doc.defaultView;
    if (!doc || !win) return;
    if (!(doc instanceof Ci.nsIDOMHTMLDocument)) return; //not html document, so its likely an xul document
    // do not try to inject scripts in chrome windows
    if (win instanceof Ci.nsIDOMChromeWindow) return;
    let url = doc.documentURI;
    if (!scacheAPI.isGoodScheme(url)) return;
    // listen for whichever kind of load event arrives first
    win.addEventListener("DOMContentLoaded", gContentLoad, true);
    win.addEventListener("load", gContentLoad, true);
    runScripts(win, true);
  } else {
    logError("observe: "+aTopic);
  }
};


////////////////////////////////////////////////////////////////////////////////
var contentObserver = new ContentObserver(); //WARNING: don't change to `let`!
var gContentLoad = contentObserver.contentLoad.bind(contentObserver); //WARNING: don't change to `let`!


////////////////////////////////////////////////////////////////////////////////
registerStartupHook("injector", function () {
  Services.obs.addObserver(contentObserver, "document-element-inserted", false);
});


registerShutdownHook("injector", function () {
  Services.obs.removeObserver(contentObserver, "document-element-inserted");
  closeStorageObjects();
});

//logStr("********* INJECTOR *********");
