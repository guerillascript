/*
 * Copyright 2015 Ketmar Dark <ketmar@ketmar.no-ip.org>
 * Portions copyright 2004-2007 Aaron Boodman
 * Contributors: See contributors list in install.rdf and CREDITS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * Note that this license applies only to the Greasemonkey extension source
 * files, not to the user scripts which it runs. User scripts are licensed
 * separately by their authors.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 */
////////////////////////////////////////////////////////////////////////////////
require("utils/utils");
let {parseMeta} = require("utils/metaparser");
let {MatchPattern, uri2re} = require("utils/matchpattern");


////////////////////////////////////////////////////////////////////////////////
function newIFile (path) {
  let fl = Cc["@mozilla.org/file/local;1"].createInstance(Ci.nsILocalFile);
  fl.initWithPath(path);
  return fl;
}
exports.newIFile = newIFile;


////////////////////////////////////////////////////////////////////////////////
function buildRelFile (fl, path) {
  if (!fl) return null;
  let res = fl.clone();
  let depthCount = 0;
  for (let n of path.split("/")) {
    if (!n || n == ".") continue;
    if (n == "..") {
      if (--depthCount < 0) return null;
      res = res.parent;
      if (!res) return null;
    } else {
      ++depthCount;
      res.append(n);
    }
  }
  return res;
}
exports.buildRelFile = buildRelFile;


////////////////////////////////////////////////////////////////////////////////
let nameExtRE = /^(.+)(\.[^.]+)$/;


////////////////////////////////////////////////////////////////////////////////
/* nfo:
 *  nsIFile file: script file object
 *  string name: script name, without ".js" ("abcd")
 *  number lmtime: lastmod time
 *  RegExp[] incREs: regular expressions for "@include"; empty array means "*"
 *  RegExp[] incMatches: regular expressions for "@match"; empty array means "no @match"
 *  RegExp[] excREs: regular expressions for "@exclude"
 *  bool unwrapped
 *  bool noframes
 *  bool wantlog: want `conlog()` and `GM_log()` APIs?
 *  string runat: run at "document-start", "document-end"
 *  nsIFile[] libs: list of required libraries
 *  nsIFile[] incs: list of required includes
 *  {string name} resources: resources dict, keyed by resource name, value is {nsIFile file, string contenttype}
 *  bool isPackage
 *  bool isUserJS
 */
function dumpNfo (nfo) {
  conlog(" ========================================");
  conlog(" file: [", nfo.file.path, "]");
  conlog(" name: [", nfo.name, "]");
  conlog(" lmtime: [", nfo.lmtime, "]");
  conlog(" isPackage: [", nfo.isPackage, "]");
  conlog(" isUserJS: [", nfo.isUserJS, "]");
  conlog(" unwrapped: [", nfo.unwrapped, "]");
  conlog(" noframes: [", nfo.noframes, "]");
  conlog(" wantlog: [", nfo.wantlog, "]");
  conlog(" runat: [", nfo.runat, "]");
  if (nfo.incREs.length) {
    conlog(" include:"); for (let re of nfo.incREs) conlog("   ", re);
  } else {
    conlog(" include: *");
  }
  if (nfo.excREs.length) { conlog(" exclude:"); for (let re of nfo.excREs) conlog("   ", re); }
  if (nfo.libs.length) { conlog(" libs:"); for (let fl of nfo.libs) conlog("   [", fl.path, "]"); }
  if (nfo.incs.length) { conlog(" incs:"); for (let fl of nfo.incs) conlog("   [", fl.path, "]"); }
  let wasOut = false;;
  for (let [name, fl] in Iterator(nfo.resources)) {
    if (!wasOut) conlog(" resources:");
    wasOut = true;
    conlog("   [", name, "]=[", fl.path, "]");
  }
}
exports.dumpScriptNfo = dumpNfo;


////////////////////////////////////////////////////////////////////////////////
let ujsdirLMT = false; // directory modify times; will become number
let userscripts = new Array(); // sorted by name
let pkgscripts = new Array(); // sorted by package name


////////////////////////////////////////////////////////////////////////////////
// return `false` if this script should be excluded
function processScriptMeta (nfo, reqtformCB) {
  function addIncLib (dir, fname, arr) {
    if (addonOptions.debugCache) conlog("lib: [", dir.path, "] : [", fname, "]");
    let ne = fname.match(nameExtRE);
    if (!ne || ne[2] != ".js") return true; // invalid file name, skip it
    let fl = buildRelFile(dir, fname);
    if (!dir.contains(fl, true)) return false; // access restricted
    if (!fl.exists()) return false;
    if (!fl.isFile() || !fl.isReadable()) return false;
    // check for duplicates
    let found = false;
    for (let ef of arr) if (ef.equals(fl)) { found = true; break; }
    if (!found) arr.push(fl);
    return true;
  }

  // return `false` if invalid or non-existing file accessed
  function procJSIncLib (dir, value, arr) {
    if (addonOptions.debugCache) conlog("libs: [", value, "] from [", dir.path, "]");
    for (let fname of value.split(/\s+/)) if (fname && !addIncLib(dir, fname, arr)) return false;
    return true;
  }

  try {
    nfo.isUserJS = nfo.isPackage || (/\.user\.js$/.test(nfo.file.leafName)); // is ".user.js" script?
    nfo.incREs = new Array();
    nfo.excREs = new Array();
    nfo.incMatches = new Array();
    nfo.unwrapped = null;
    nfo.noframes = (nfo.isUserJS ? false : true); // default
    nfo.wantlog = null;
    nfo.runat = "document-end"; // default
    nfo.libs = new Array();
    nfo.incs = new Array();
    nfo.resources = {};
    let text = fileReadText(nfo.file);
    if (!text) return false;
    let meta = parseMeta(text);
    if (meta.length == 0) return false;
    //conlog("got meta for ", nfo.file.path);
    // parse meta fields
    let incAll = false;
    let wasMatch = false;
    let wasGrantNone = false;
    let wasGrantLog = false;
    let wasGrantAnything = false;
    for (let kv of meta) {
      if (kv.name === "disabled") {
        // this script is disabled
        if (addonOptions.debugCache) conlog("@disabled");
        return false;
      }
      if (kv.name === "exclude") {
        if (!kv.value) continue;
        if (kv.value === "*") {
          // excluded for all uris
          if (addonOptions.debugCache) conlog("@exclude *");
          return false;
        }
        let re = uri2re(kv.value);
        if (re) nfo.excREs.push(re);
        continue;
      }
      if (kv.name === "include") {
        if (!incAll) {
          if (!kv.value) continue;
          if (kv.value === "*") {
            incAll = true;
            wasMatch = false;
            if (nfo.incREs.length != 0) nfo.incREs = new Array();
            if (nfo.incMatches.length != 0) nfo.incMatches = new Array();
          } else {
            let re = uri2re(kv.value);
            if (re) nfo.incREs.push(re);
          }
        }
        continue;
      }
      if (kv.name === "match") {
        if (!incAll) {
          if (!kv.value) continue;
          try {
            let mt = new MatchPattern(kv.value);
            if (mt.all) {
              //if (addonOptions.debugCache) conlog("@match <all_urls>");
              incAll = true;
              wasMatch = false;
              if (nfo.incREs.length != 0) nfo.incREs = new Array();
              if (nfo.incMatches.length != 0) nfo.incMatches = new Array();
            } else {
              //if (addonOptions.debugCache) conlog("@match: ", mt.pattern);
              wasMatch = true;
              nfo.incMatches.push(mt);
            }
          } catch (e) {}
        }
        continue;
      }
      if (kv.name === "run-at") {
        switch (kv.value) {
          case "document-start":
          case "document-end":
            nfo.runat = kv.value;
            break;
        }
        continue;
      }
      if (kv.name === "unwrap" || kv.name === "unwrapped") {
        nfo.unwrapped = true;
        continue;
      }
      if (kv.name === "wrap" || kv.name === "wrapped") {
        nfo.unwrapped = false;
        continue;
      }
      if (kv.name === "noframe" || kv.name === "noframes") {
        nfo.noframes = false;
        continue;
      }
      if (kv.name === "frame" || kv.name === "frames") {
        nfo.noframes = true;
        continue;
      }
      if (kv.name === "library" || kv.name === "libraries") {
        if (nfo.isPackage) continue;
        if (!procJSIncLib(getUserLibDir(), kv.value, nfo.libs)) {
          if (addonOptions.debugCache) conlog("bad @libraries");
          return false;
        }
        continue;
      }
      if (kv.name === "require" || kv.name === "requires") {
        if (nfo.isPackage) continue;
        let dir = nfo.file.parent; // remove script name
        dir.append(nfo.name);
        if (nfo.isPackage) {
          // special processing for scripts from packages
          if (kv.name !== "require" || !kv.value) continue;
          if (typeof(reqtformCB) !== "function") continue;
          let v = reqtformCB(nfo, kv.value);
          if (typeof(v) === "undefined") continue;
          if (v === false) return false; // alas
          if (!v || typeof(v) !== "string") continue;
          if (!addIncLib(dir, v, nfo.incs)) return false;
        } else {
          if (!procJSIncLib(dir, kv.value, nfo.incs)) {
            if (addonOptions.debugCache) conlog("bad @requires");
            return false;
          }
        }
        continue;
      }
      if (kv.name === "grant") {
        // "@grant none" means the same as "@unwrap"
        switch (kv.value) {
          case "none": wasGrantNone = true; break;
          case "GM_log": wasGrantLog = true; break;
          case "unsafeWindow": break;
          default: if (kv.value) wasGrantAnything = true; break;
        }
        continue;
      }
    }
    // met any includes?
    if (!incAll && !wasMatch && nfo.incREs.length == 0) {
      // no includes at all
      if (addonOptions.debugCache) conlog("no @include");
      return false;
    }
    // fix access rights
    if (wasGrantLog) {
      nfo.wantlog = true;
    } else {
      if (nfo.wantlog === null) nfo.wantlog = (!nfo.isPackage && !nfo.isUserJS);
    }
    // if we met "@grant none", this means "unwrap"
    if (wasGrantNone) {
      if (nfo.unwrapped === null) nfo.unwrapped = true;
    } else if (wasGrantAnything) {
      // other "@grant"s means "wrapped"
      if (nfo.unwrapped === null) nfo.unwrapped = false;
    } else {
      // nothing was specified
      if (nfo.unwrapped === null) nfo.unwrapped = false;
    }
    // public `conlog` function for unwrapped scripts
    if (nfo.unwrapped) nfo.wantlog = true;
    return true;
  } catch (e) {
    logException("processScriptMeta", e);
  }
  return false;
}
exports.processScriptMeta = processScriptMeta;


////////////////////////////////////////////////////////////////////////////////
// bool forced: do force scan (i.e. ignore jsdirLMT value)
function scanUJSDirectory (forced) {
  //if (addonOptions.debugCache) conlog("updating cache: '"+dir.path+"'");
  let dir = getUserJSDir();
  if (!dir.exists()) { userscripts = new Array(); return; }
  if (!dir.isDirectory()) { userscripts = new Array(); return; }
  let lmtime = dir.lastModifiedTime;
  if (!forced && lmtime === ujsdirLMT) return; // do nothing, as nothing was changed
  // rescan
  ujsdirLMT = lmtime;
  userscripts = new Array();
  let en = dir.directoryEntries;
  while (en.hasMoreElements()) {
    let fl = en.getNext().QueryInterface(Ci.nsILocalFile);
    if (!fl.exists()) continue;
    if (!fl.isFile() || !fl.isReadable()) continue;
    // check file name
    let name = fl.leafName;
    if (name[0] == "_") continue; // disabled
    let ne = name.match(nameExtRE);
    if (!ne || ne[2] != ".js") continue; // invalid extension
    // create script nfo object
    if (addonOptions.debugCache) conlog("trying ujs '", name, "'");
    let nfo = {};
    nfo.isPackage = false;
    nfo.file = fl.clone();
    nfo.name = ne[1]; // base name (w/o extension)
    nfo.lmtime = fl.lastModifiedTime;
    if (!processScriptMeta(nfo)) continue; // skip this script
    if (addonOptions.debugCache) conlog("adding ujs '", name, "'");
    //dumpNfo(nfo);
    userscripts.push(nfo);
  }
  // sort scripts
  userscripts.sort(function (a, b) {
    let ap = a.file.leafName;
    let bp = b.file.leafName;
    if (ap < bp) return -1;
    if (ap > bp) return 1;
    return 0;
  });
}


////////////////////////////////////////////////////////////////////////////////
function scanPackages () {
  pkgscripts = new Array(); // sorted by package name
  for (let pi of pkgDB.getActivePackagesForCache()) {
    if (addonOptions.debugCache) conlog("*pkg: pi.path=[", pi.path, "]");
    let fl = newIFile(pi.path);
    if (!fl.exists()) continue;
    if (!fl.isFile() || !fl.isReadable()) continue;
    let name = fl.leafName;
    //if (name[0] == "_") continue; // disabled
    let ne = name.match(nameExtRE);
    if (!ne || ne[2] != ".js") continue; // invalid extension
    // create script nfo object
    if (addonOptions.debugCache) conlog("trying pkg '", fl.path, "'");
    let nfo = {};
    nfo.isPackage = true;
    nfo.file = fl.clone();
    nfo.name = ne[1]; // base name (w/o extension)
    nfo.lmtime = fl.lastModifiedTime;
    if (!processScriptMeta(nfo)) continue; // skip this script
    if (addonOptions.debugCache) conlog("added pkg '", fl.path, "'");
    //conlog("path: ", pi.path);
    // add includes
    nfo.incs = new Array();
    for (let ifn of pi.reqs) {
      //conlog("  req: [", ifn, "]");
      if (addonOptions.debugCache) conlog("  ifn=[", ifn, "]");
      let ifl = newIFile(ifn);
      if (!ifl.exists()) continue;
      if (!ifl.isFile() || !ifl.isReadable()) continue;
      let mt = ifl.leafName.match(nameExtRE);
      if (!mt || mt[2] != ".js") continue; // invalid extension
      nfo.incs.push(ifl);
    }
    // add resources
    nfo.resources = {};
    for (let [name, rsrc] in Iterator(pi.rsrc)) {
      if (!name) continue; // just in case
      //conlog("  res: [", name, "] is [", rsrc.path, "] : [", rsrc.contenttype, "]");
      if (addonOptions.debugCache) conlog("  rsrc.path=[", rsrc.path, "]");
      let rfl = newIFile(rsrc.path);
      if (!rfl.exists()) continue;
      if (!rfl.isFile() || !rfl.isReadable()) continue;
      nfo.resources[name] = {file:rfl, contenttype:rsrc.contenttype};
    }
    //dumpNfo(nfo);
    pkgscripts.push(nfo);
  }
  //conlog("=== pkgscripts ==="); for (let nfo of pkgscripts) dumpNfo(nfo);
}


////////////////////////////////////////////////////////////////////////////////
exports.reset = function () {
  if (addonOptions.debugCache) conlog("resetting script cache");
  ujsdirLMT = false;
  userscripts = new Array(); // sorted by name
  pkgscripts = new Array(); // sorted by package name
};


////////////////////////////////////////////////////////////////////////////////
exports.dumpCache = function () {
  conlog("=== userscripts ==="); for (let nfo of userscripts) dumpNfo(nfo);
  conlog("=== pkgscripts ==="); for (let nfo of pkgscripts) dumpNfo(nfo);
};


////////////////////////////////////////////////////////////////////////////////
// bool forced: do force scan (i.e. ignore jsdirLMT value)
function refreshCache (forced) {
  if (ujsdirLMT === false) forced = true;
  scanUJSDirectory(forced);
  if (forced) scanPackages();
};
exports.refreshCache = refreshCache;


////////////////////////////////////////////////////////////////////////////////
exports.isGoodScheme = function (scstr) {
  let cpos = scstr.indexOf(":");
  if (cpos < 0) return false;
  scstr = scstr.substr(0, cpos);
  switch (scstr) {
    case "http":
    case "https":
    case "ftp": // no, really, why?
    case "data": // inject into frames and other smart things created with "data:" URL
    case "file":
      return true;
    default:
      return false;
  }
};


////////////////////////////////////////////////////////////////////////////////
function isGoodForUrl (nfo, url, inFrame, runat) {
  if (!nfo) return false;
  if (inFrame && nfo.noframes) return false;
  if (runat !== nfo.runat) return false;
  // check excludes
  if (nfo.excREs.length) {
    let hit = false;
    for (let re of nfo.excREs) if (re.test(url)) { hit = true; break; }
    if (hit) return false;
  }
  // check includes
  if (nfo.incREs.length === 0) return true; // "@include *"
  if (nfo.incREs.length) {
    let hit = false;
    for (let re of nfo.incREs) if (re.test(url)) { hit = true; break; }
    if (hit) return true;
  }
  // check matches
  if (nfo.incMatches.length) {
    let hit = false;
    for (let mt of nfo.incMatches) if (mt.doMatch(url)) { hit = true; break; }
    if (hit) return true;
  }
  return false;
}


// string uri
// returns array of nfos or false
// url: string
// runat: string
exports.scriptsForUrl = function (url, inFrame, runat) {
  let res = new Array();
  inFrame = !!inFrame;
  refreshCache();
  // userscripts
  for (let nfo of userscripts) {
    if (!isGoodForUrl(nfo, url, inFrame, runat)) continue;
    res.push(nfo);
  }
  // package scripts
  for (let nfo of pkgscripts) {
    if (!isGoodForUrl(nfo, url, inFrame, runat)) continue;
    res.push(nfo);
  }
  return res;
};


////////////////////////////////////////////////////////////////////////////////
// get script list; returns array of xnfo
// xnfo:
//  string path
//  string name
//  bool islib;
(function (global) {
  let list = new Array();

  let jdLMT = false;
  let ldLMT = false;
  let lastModCount = 0;

  function scanDir (dir, islib) {
    if (!dir.exists()) return;
    if (!dir.isDirectory()) return;
    let en = dir.directoryEntries;
    while (en.hasMoreElements()) {
      let fl = en.getNext().QueryInterface(Ci.nsILocalFile);
      if (!fl.exists()) continue;
      if (!fl.isFile() || !fl.isReadable()) continue;
      let ne = fl.leafName.match(nameExtRE);
      if (!ne || ne[2] != ".js") continue;
      let xnfo = {};
      xnfo.path = fl.path;
      xnfo.name = (islib ? "libs/" : "")+fl.leafName;
      xnfo.islib = islib;
      //conlog("getScriptsForEdit: '", xnfo.name, "' [", xnfo.path, "]");
      list.push(xnfo);
    }
  }

  global.getScriptsForEdit = function () {
    let jdir = getUserJSDir();
    let ldir = getUserLibDir();

    if (jdLMT !== jdir.lastModifiedTime || ldLMT !== ldir.lastModifiedTime) {
      ++lastModCount;
      jdLMT = jdir.lastModifiedTime;
      ldLMT = ldir.lastModifiedTime;
      list = new Array();
      scanDir(jdir, false);
      scanDir(ldir, true);
    }

    return {lmod: lastModCount, list:list};
  };
})(this);
exports.getScriptsForEdit = getScriptsForEdit;
