/* coded by Ketmar // Invisible Vector (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/txt/copying/ for more details.
 */
////////////////////////////////////////////////////////////////////////////////
/*
/ *let gcli =* / uses("resource://gre/modules/devtools/gcli.jsm");
//uses("resource://gre/modules/devtools/gcli");
//let gcli = require("resource://gre/modules/devtools/gcli.jsm");

require("utils/utils");
let {oneShotTimer, intervalTimer} = require("utils/timer");


////////////////////////////////////////////////////////////////////////////////
function wild2re (wildstr) {
  wildstr = wildstr.replace(/^\s+/, "").replace(/\s+$/, "");
  if (wildstr == "") wildstr = "*";
  if (wildstr == "*") return new RegExp(/./);
  // regexp?
  if (wildstr.length >= 2 && wildstr.charCodeAt(0) == 47 && wildstr.charCodeAt(wildstr.length-1) == 47) {
    if (wildstr.length < 3) return new RegExp(/./);
    return new RegExp(wildstr.substring(1, wildstr.length-1), "i");
  }
  // convert glob to regexp
  let re = "^";
  for (let f = 0; f < wildstr.length; ++f) {
    switch (wildstr[f]) {
      case "*": re += ".*"; break; // any, greedy
      case ".": case "?": case "^": case "$": case "+":
      case "{": case "}": case "[": case "]": case "|":
      case "(": case ")": case "\\":
        re += "\\"+wildstr[f];
        break;
      case " ": re += "\\s"; break;
      default: re += wildstr[f]; break;
    }
  }
  re += "$";
  //if (addonOptions.debugCache) conlog("wildstr:["+wildstr+"]  re: ["+re+"]");
  return new RegExp(re, "i");
}


////////////////////////////////////////////////////////////////////////////////
let editList = {lmod:-1};
let editNames;


function getEditList () {
  let cur = scacheAPI.getScriptsForEdit();
  if (cur.lmod != editList.lmod) {
    editList = {lmod:cur.lmod, list:cur.list};
    editNames = new Array();
    for (let xnfo of editList.list) editNames.push({name:xnfo.name, value:xnfo.path});
  }
  return editNames;
}


////////////////////////////////////////////////////////////////////////////////
let pkgList = {lmod:-1};
let pkgNames;


function getPkgList () {
  let cur = pkgman.getPackageList();
  //conlog("getPkgList: pkgList.lmod=", pkgList.lmod, "; cur.lmod=", cur.lmod);
  if (cur.lmod != pkgList.lmod) {
    pkgList = {lmod:cur.lmod, list:cur.list};
    pkgNames = new Array();
    for (let pi of pkgList.list) pkgNames.push({name:pi.name, value:pi});
  }
  return pkgNames;
}


////////////////////////////////////////////////////////////////////////////////
function addjsext (fn) {
  if (!/\.js$/.test(fn)) fn += ".js";
  return fn;
}


////////////////////////////////////////////////////////////////////////////////
function openEditor (fname, chromeWin, newfile) {
  let fl = Cc["@mozilla.org/file/local;1"].createInstance(Ci.nsIFile);
  fl.followLinks = true;
  fl.initWithPath(fname);
  let lmtime = 0;
  if (fl.exists()) {
    if (fl.isDirectory()) return;
    lmtime = fl.lastModifiedTime;
  } else {
    if (!newfile) return;
  }
  let text;
  try {
    text = fileReadText(fl);
    newfile = false;
  } catch (e) {
    if (!newfile) {
      logError("can't read file: "+fname);
      return;
    }
    text =
      "// ==UserScript==\n"+
      "// @name        \n"+
      "// @description \n"+
      "// @version     1.0\n"+
      "// @include     *\n"+
      "// @run-at      document-end\n"+
      "// @noframes\n"+
      "// @nowrap\n"+
      "// @libraries   \n"+
      "// @require     \n"+
      "// ==/UserScript==\n\n";
  }
  let spw = chromeWin.Scratchpad.ScratchpadManager.openScratchpad({
    filename: fname,
    text: text,
    saved: !newfile,
  });
  // TODO: how can i observe file changes without timer?
  let wasChange = false;
  let checkFC = function () {
    let fl = Cc["@mozilla.org/file/local;1"].createInstance(Ci.nsIFile);
    fl.followLinks = true;
    fl.initWithPath(fname);
    if (fl.exists() && !fl.isDirectory() && lmtime != fl.lastModifiedTime) {
      lmtime = fl.lastModifiedTime;
      return true;
    }
    return false;
  };
  let tm = intervalTimer(function () {
    if (checkFC()) { scacheAPI.reset(); wasChange = true; }
  }, 1500);
  // kill timer on closing
  spw.addEventListener("close", function sploaded() {
    spw.removeEventListener("close", sploaded, false);
    tm.cancel();
    // just in case
    if (wasChange || checkFC()) {
      scacheAPI.reset();
      scacheAPI.validate();
      latestUC = scacheAPI.getUC();
    }
  }, false);
}


////////////////////////////////////////////////////////////////////////////////
let gcliCommandSpecs = [
  {
    name: "guerilla",
    description: "GuerillaJS control",
  },
  // subcommands
  {
    name: "guerilla about",
    description: "show various info",
    params: [
      {
        name: "type",
        description: "info to show",
        type: { name: "selection", data: ["readme", "credits", "thanks", "licenses"] },
        defaultValue: "readme",
      }
    ],
    exec: function (args, context) {
      let bro = context.environment.chromeWindow.gBrowser;
      let list;
      switch (args.type) {
        case "credits": list = "CREDITS.txt"; break;
        case "thanks": list = "THANKS.txt"; break;
        case "licenses": list = ["LICENSE.bsd.txt", "LICENSE.mit.txt", "LICENSE.mpl.txt", "LICENSE.wtfpl.txt"]; break;
        default: list = "README.txt"; break;
      }
      if (typeof(list) == "string") {
        bro.selectedTab = bro.addTab(gsdoxUrl+list);
      } else {
        for (let name of list) bro.selectedTab = bro.addTab(gsdoxUrl+name);
      }
    },
  },
  //
  {
    name: "guerilla reset",
    description: "reset all internal caches",
    exec: function (args, context) {
      //conlog("clearing guerilla caches...");
      scacheAPI.reset();
    },
  },
  //
  {
    name: "guerilla debug",
    description: "switch debug mode on/off",
    params: [
      {
        name: "flag",
        description: "action to perform",
        type: { name: "selection", data: ["show", "tan", "ona", "toggle"] },
        defaultValue: "show",
      }
    ],
    returnValue: "string",
    exec: function (args, context) {
      switch (args.flag) {
        case "tan": addonOptions.debugMode = true; break;
        case "ona": addonOptions.debugMode = false; break;
        case "toggle": addonOptions.debugMode = !addonOptions.debugMode; break;
      }
      return "guerilla debug mode is "+(addonOptions.debugMode ? "on" : "off");
    },
  },
  //
  {
    name: "guerilla log",
    description: "switch logging on/off",
    params: [
      {
        name: "flag",
        description: "action to perform",
        type: { name: "selection", data: ["show", "tan", "ona", "toggle"] },
        defaultValue: "show",
      }
    ],
    returnValue: "string",
    exec: function (args, context) {
      switch (args.flag) {
        case "tan": addonOptions.logEnabled = true; break;
        case "ona": addonOptions.logEnabled = false; break;
        case "toggle": addonOptions.logEnabled = !addonOptions.logEnabled; break;
      }
      return "guerilla logging is "+(addonOptions.logEnabled ? "on" : "off");
    },
  },
  //
  {
    name: "guerilla state",
    description: "switch guerilla state",
    params: [
      {
        name: "flag",
        description: "action to perform",
        type: { name: "selection", data: ["show", "active", "inactive", "toggle"] },
        defaultValue: "show",
      }
    ],
    returnValue: "string",
    exec: function (args, context) {
      switch (args.flag) {
        case "active": addonOptions.active = true; break;
        case "inactive": addonOptions.active = false; break;
        case "toggle": addonOptions.active = !addonOptions.active; break;
      }
      return "guerilla is "+(addonOptions.active ? "" : "in")+"active";
    },
  },
  //
  {
    name: "guerilla activate",
    description: "activate guerilla",
    returnValue: "string",
    exec: function (args, context) {
      addonOptions.active = true;
      return "guerilla is active";
    },
  },
  //
  {
    name: "guerilla deactivate",
    description: "deactivate guerilla",
    returnValue: "string",
    exec: function (args, context) {
      addonOptions.active = false;
      return "guerilla is inactive";
    },
  },
  //
  {
    name: "guerilla new",
    description: "create new userscript",
    params: [
      {
        name: "filename",
        description: "new script name",
        type: "string",
      }
    ],
    exec: function (args, context) {
      let fname = args.filename;
      let mt = fname.match(/^\/?libs\/([^\/]+)$/);
      if (mt) fname = mt[1];
      if (fname.length == 0 || fname.indexOf("/") >= 0) return;
      fname = addjsext(fname);
      let dir;
      if (mt) {
        // new library
        dir = getUserLibDir();
        dir.append(fname);
      } else {
        //if (args.filename.length == 0 || args.filename.indexOf("/") >= 0) { alert("invalid file name: "+args.filename); return; }
        dir = getUserJSDir();
        dir.append(fname);
      }
      conlog("trying to edit '"+dir.path+"'");
      openEditor(dir.path, context.environment.chromeDocument.defaultView, true);
    },
  },
  //
  {
    name: "guerilla edit",
    description: "edit guerilla script",
    //returnValue: "string",
    params: [
      {
        name: "filename",
        description: "script name to edit",
        type: {
          name: "selection",
          cacheable: false,
          lookup: function (context) getEditList(),
        }, // type
      }
    ],
    exec: function (args, context) {
      if (args.filename) openEditor(args.filename, context.environment.chromeDocument.defaultView);
    },
  },
  // package backend
  {
    name: "guerilla package",
    description: "GuerillaJS package control",
  },
  {
    name: "guerilla package abort",
    description: "abort all operations",
    exec: function (args, context) { pkgman.cancel(); },
  },
  {
    name: "guerilla package list",
    description: "list installed packages",
    returnValue: "string",
    params: [
      {
        name: "mask",
        description: "mask",
        defaultValue: "*",
        type: "string",
      }
    ],
    exec: function (args, context) {
      let mask = args.mask||"*";
      let re = wild2re(mask);
      let res = "=== package list ===";
      let count = 0;
      for (let pi of pkgDB.getActivePackages()) {
        if (re.test(pi.name)) {
          res += "\n"+pi.name;
          if (pi.version) res += "\t"+pi.version;
          ++count;
        }
      }
      //return (count ? res : "no packages found");
      //TODO: special output
      logError(count ? res : "no packages found");
      return "find result in error console";
    },
  },
  {
    name: "guerilla package update",
    description: "update package(s)",
    params: [
      {
        name: "mask",
        description: "mask",
        defaultValue: "",
        type: "string",
      }
    ],
    exec: function (args, context) {
      if (!args.mask) return;
      let mask = args.mask||"*";
      let re = wild2re(mask);
      for (let pi of pkgDB.getActivePackages()) {
        if (re.test(pi.name)) {
          try {
            pkgman.update(pi.name);
          } catch (e) {
            logError("ERROR: ", e.message);
          }
        }
      }
    },
  },
  {
    name: "guerilla package install",
    description: "install package",
    params: [
      {
        name: "name",
        description: "package name",
        type: "string",
      },
      {
        name: "url",
        description: "package install url",
        type: "string",
      }
    ],
    exec: function (args, context) {
      if (!args.name || !args.url) return;
      try {
        pkgman.install(args.name, args.url);
      } catch (e) {
        logError("ERROR: ", e.message);
      }
    },
  },
  {
    name: "guerilla package remove",
    description: "remove package",
    params: [
      {
        name: "name",
        description: "package name",
        type: {
          name: "selection",
          cacheable: false,
          lookup: function (context) getPkgList(),
        }, // type
      }
    ],
    exec: function (args, context) {
      //if (!args.name.name) return;
      try {
        pkgman.remove(args.name.name);
      } catch (e) {
        logError("ERROR: ", e.message);
      }
    },
  }
];


////////////////////////////////////////////////////////////////////////////////
registerStartupHook("gcli", function () {
  try {
    //if (typeof(gcli) === "undefined") return;
    //if (typeof(gcli.addCommand) !== "function") return;
    for (let cmd of gcliCommandSpecs) {
      if (cmd && typeof(cmd) === "object" && cmd.name) gcli.addItems([cmd]);
    }
  } catch (e) {
    logError("ERROR: ", e.message);
  }
});


registerShutdownHook("gcli", function () {
  try {
    //if (typeof(gcli) === "undefined") return;
    //if (typeof(gcli.removeCommand) !== "function") return;
    for (let cmd of gcliCommandSpecs) {
      if (cmd && typeof(cmd) === "object" && cmd.name) gcli.removeItems([cmd]);
    }
  } catch (e) {
    logError("ERROR: ", e.message);
  }
});
*/
