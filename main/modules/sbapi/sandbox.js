/*
 * Portions copyright 2004-2007 Aaron Boodman
 * Copyright 2015 Ketmar Dark <ketmar@ketmar.no-ip.org>
 * Contributors: See contributors list in install.rdf and CREDITS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * Note that this license applies only to the Greasemonkey extension source
 * files, not to the user scripts which it runs. User scripts are licensed
 * separately by their authors.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 */
////////////////////////////////////////////////////////////////////////////////
let {GuerillaXmlHttpReqester} = require("sbapi/xmlhttprequest");
let {ScriptStorage} = require("sbapi/scriptstorage");
let {openTab} = require("sbapi/opentab");
let {buildRelFile} = require("scriptcache");


////////////////////////////////////////////////////////////////////////////////
function fileReadBinary (fl) {
  let istream = Cc["@mozilla.org/network/file-input-stream;1"].createInstance(Ci.nsIFileInputStream);
  istream.init(fl, -1, -1, false);
  let bstream = Cc["@mozilla.org/binaryinputstream;1"].createInstance(Ci.nsIBinaryInputStream);
  bstream.setInputStream(istream);
  let bytes = bstream.readBytes(bstream.available());
  bstream.close();
  return bytes;
}


////////////////////////////////////////////////////////////////////////////////
let ioSvc = Cc["@mozilla.org/network/io-service;1"].getService(Ci.nsIIOService);

function uriFromUrl (url, base) {
  let baseUri = null;
  if (typeof(base) === "string") {
    baseUri = uriFromUrl(base);
  } else if (base) {
    baseUri = base;
  }
  try {
    return ioSvc.newURI(url, null, baseUri);
  } catch (e) {
    return null;
  }
}


let newFileURI = Cc["@mozilla.org/network/io-service;1"].getService(Ci.nsIIOService).newFileURI;


////////////////////////////////////////////////////////////////////////////////
// string aString: A string of data to be hashed.
// string aAlg: optional; the hash algorithm to be used; possible values are: MD2, MD5, SHA1, SHA256, SHA384, and SHA512; defaults to SHA1
// string aCharset: optional; the charset used by the passed string; defaults to UTF-8
function cryptoHashStr (aString, aAlg, aCharset) {
  const PR_UINT32_MAX = 0xffffffff; // this tells updateFromStream to read the entire string

  let str = ""+aString;
  let alg = (""+(aAlg||"SHA1")).trim().toUpperCase();
  let charset = (""+(aCharset||"UTF-8")).trim();

  let chashObj = Cc["@mozilla.org/security/hash;1"].createInstance(Ci.nsICryptoHash);

  try {
    chashObj.initWithString(alg);
  } catch (e) {
    logError("invalid hash algorithm: '"+aAlg+"'");
    throw new Error("invalid hash algorithm: '"+aAlg+"'");
  }

  let uniconvObj = Cc["@mozilla.org/intl/scriptableunicodeconverter"].createInstance(Ci.nsIScriptableUnicodeConverter);
  try {
    uniconvObj.charset = charset;
  } catch (e) {
    logError("invalid charset: '"+aCharset+"'");
    throw new Error("invalid charset: '"+aCharset+"'");
  }

  if (str) chashObj.updateFromStream(uniconvObj.convertToInputStream(str), PR_UINT32_MAX);
  let hash = chashObj.finish(false); // hash as raw octets
  //return [("0"+hash.charCodeAt(i).toString(16)).slice(-2) for (i in hash)].join("");
  var res = "";
  for (var i = 0; i < hash.length; ++i) res += ("0"+hash.charCodeAt(i).toString(16)).slice(-2);
  return res;
}


////////////////////////////////////////////////////////////////////////////////
function safeHTMLParser (domWin, htmlstr, baseUrl) {
  //conlog("domWin: "+domWin);
  //conlog("htmlstr: "+htmlstr);
  //conlog("baseUrl: "+baseUrl);
  let doc = domWin.document.implementation.createDocument("", "", domWin.document.implementation.createDocumentType("html", "", ""));
  doc.appendChild(doc.createElement("html"));
  doc.documentElement.appendChild(doc.createElement("body"));

  let baseUri = null, frag;
  if (typeof(baseUrl) !== "undefined") baseUri = uriFromUrl(baseUrl);

  let pu = Cc["@mozilla.org/parserutils;1"].createInstance(Ci.nsIParserUtils);
  if (!pu) { logError("FUCKED!"); return null; }

  frag = pu.parseFragment(htmlstr, 0, false, baseUri, doc.body);
  doc.adoptNode(frag);
  doc.body.appendChild(frag);
  return doc;
}


////////////////////////////////////////////////////////////////////////////////
function genUUID () {
  let uuidgen = Cc["@mozilla.org/uuid-generator;1"].createInstance(Ci.nsIUUIDGenerator);
  if (!uuidgen) throw new Error("no UUID generator available!");
  return uuidgen.generateUUID().toString();
}


////////////////////////////////////////////////////////////////////////////////
let storageObjects = {};


function getStorageObject (nfo) {
  if (nfo.name in storageObjects) {
    return storageObjects[nfo.name];
  } else {
    let res = new ScriptStorage(nfo);
    storageObjects[nfo.name] = res;
    return res;
  }
}


exports.closeStorageObjects = function () {
  for (let k in storageObjects) {
    if (typeof(k) != "string") continue;
    let dbo = storageObjects[k];
    //if (typeof(dbo) != "object") continue;
    if (dbo.opened) {
      debuglog("freeing storage object for '", k, "'");
      dbo.close();
    }
  }
  storageObjects = {};
};


////////////////////////////////////////////////////////////////////////////////
exports.createSandbox = function (domWin, nfo, url) {
  let sandbox;
  let scres = nfo.resources;

  if (nfo.unwrapped) {
    // create "unwrapped" sandbox
    sandbox = Cu.Sandbox(domWin, {
      sandboxName: "unwrapped",
      sameZoneAs: domWin, // help GC a little
      sandboxPrototype: domWin/*.wrappedJSObject*/,
      wantXrays: false,
    });
    // alias unsafeWindow for compatibility
    Cu.evalInSandbox("const unsafeWindow = window;", sandbox);
    //sandbox.isWrapped = !nfo.unwrapped;
  } else {
    // create "real" sandbox
    sandbox = Cu.Sandbox(domWin, {
      sandboxName: nfo.name,
      sameZoneAs: domWin, // help GC a little
      sandboxPrototype: domWin/*.wrappedJSObject*/,
      wantXrays: true,
    });

    //sandbox.isWrapped = !nfo.unwrapped;
    // no need for the "bugfix" from GM, as Pale Moon doesn't need it
    //!sandbox.unsafeWindow = domWin.wrappedJSObject;
    // alas, Tycho inherited this bug from the new Ff, so reapply it
    /*
    var uwgok = false;
    try {
      var unsafeWindowGetter = new sandbox.Function("return window.wrappedJSObject||window;");
      Object.defineProperty(sandbox, "unsafeWindow", {get: unsafeWindowGetter});
      uwgok = true;
    } catch (e) {
    }
    */
    /*
    if (!uwgok) {
      try {
        //sandbox.unsafeWindow = domWin.wrappedJSObject;
        var unsafeWinObj = domWin.wrappedJSObject;
        var domwinobj = domWin;
        var unsafeWindowGetter = function () {
          logError("unsafeWindowGetter: "+domwinobj);
          return domwinobj;
        };
        Object.defineProperty(sandbox, "unsafeWindow", {get: unsafeWindowGetter});
        logError("usfw: "+domWin.wrappedJSObject);
        uwgok = true;
      } catch (e) {
        logError("XERR: "+e.message);
      }
    }
    */
    try {
      Cu.evalInSandbox("const unsafeWindow = window.wrappedJSObject||window;", sandbox, "ECMAv5", "__sandbox_creator__", 1);
    } catch (e) {
      logError("XERR: "+e.message);
    }

    // functions for interaction with unsafeWindow; see: http://goo.gl/C8Au16
    sandbox.createObjectIn = Cu.createObjectIn;
    sandbox.cloneInto = Cu.cloneInto;
    sandbox.exportFunction = Cu.exportFunction;

    sandbox.GM_generateUUID = tieto(null, genUUID);
    sandbox.GM_cryptoHash = tieto(null, cryptoHashStr);

    //Object.defineProperty(sandbox, "GM_safeHTMLParser", {get: function () tieto(null, safeHTMLParser, domWin)});
    sandbox.GM_safeHTMLParser = tieto(null, safeHTMLParser, domWin);

    let scriptStorage = getStorageObject(nfo);
    sandbox.GM_getValue = tieto(scriptStorage, "getValue");
    sandbox.GM_setValue = tieto(scriptStorage, "setValue");
    sandbox.GM_deleteValue = tieto(scriptStorage, "deleteValue");
    //sandbox.GM_listValues = tieto(scriptStorage, "listValues");
    sandbox.GM_listValues = tieto(null, function (stg, sbox) {
      //scriptStorage, "listValues");
      let res = scriptStorage.listValues();
      if (res) {
        res = Cu.cloneInto(res, sbox);
      }
      return res;
    }, scriptStorage, sandbox);

    sandbox.GM_xmlhttpRequest = tieto(new GuerillaXmlHttpReqester(domWin, url, sandbox), "contentStartRequest");

    sandbox.GM_addStyle = tieto(null, function (doc, cssstr) {
      var head = doc.getElementsByTagName("head")[0];
      if (head) {
        var style = doc.createElement("style");
        style.textContent = cssstr;
        style.type = "text/css";
        head.appendChild(style);
        return style;
      }
      return null;
    }, domWin.document);

    sandbox.GM_openInTab = tieto(null, openTab, domWin);

    sandbox.GM_getResourceText = tieto(null, function (name) {
      if (typeof(name) === "undefined") throw new Error("GM_getResourceText(): no name given!");
      name = ""+name;
      let rsrc = scres[name];
      if (!rsrc) throw new Error("GM_getResourceText(): no resource found: '"+name+"'");
      return fileReadText(rsrc.file);
    });

    sandbox.GM_getResourceURL = tieto(null, function (name) {
      //logError("GM_getResourceURL(): stub!");
      //throw new Error("GM_getResourceURL() not implemented");
      if (typeof(name) === "undefined") throw new Error("GM_getResourceText(): no name given!");
      name = ""+name;
      let rsrc = scres[name];
      if (!rsrc) throw new Error("GM_getResourceText(): no resource found: '"+name+"'");
      let rawdata = fileReadBinary(rsrc.file);
      return "data:"+rsrc.contenttype+";base64,"+encodeURIComponent(btoa(rawdata));
    });

    // stubs
    sandbox.GM_registerMenuCommand = tieto(null, function () { logError("GM_registerMenuCommand(): stub!"); });
    sandbox.GM_setClipboard = tieto(null, function () { logError("GM_setClipboard(): stub!"); });
  }

  // provide log functions for both wrapped and unwrapped scripts
  if (!nfo.unwrapped || nfo.wantlog) {
    sandbox.conlog = tieto(null, conlog);
    sandbox.logError = tieto(null, logError);
    if (!nfo.unwrapped) sandbox.GM_log = tieto(null, conlog);
  }

  Object.defineProperty(sandbox, "GM_info", {
    get: tieto(null, function () { logError("GM_info(): stub!"); return {}; }),
  });

  // nuke sandbox when this window unloaded
  domWin.addEventListener("unload", function () {
    if (sandbox) {
      debuglog("**** NUKING SANDBOX *** [", nfo.name, "] : [", url, "]");
      Cu.nukeSandbox(sandbox);
      sandbox = null;
    }
  }, true);


  return sandbox;
}


////////////////////////////////////////////////////////////////////////////////
exports.runInSandbox = function (sandbox, nfo) {
  // eval the code, with anonymous wrappers when/if appropriate
  function evalLazyWrap (code, fileName, dowrap) {
    try {
      Cu.evalInSandbox(code, sandbox, "ECMAv5", fileName, 1);
    } catch (e) {
      if (dowrap && ("return not in function" == e.message)) {
        // we never anon wrap unless forced to by a "return not in a function" error
        logError("please, do not use `return` in top-level code in "+fileName+":"+e.lineNumber);
        Cu.evalInSandbox("(function(){ "+code+"\n})()", sandbox, "ECMAv5", fileName, 1);
      } else {
        // otherwise raise
        throw e;
      }
    }
  }

  // eval the code, with a try/catch to report errors cleanly
  function evalNoThrow (code, fileName, dowrap) {
    dowrap = !!dowrap;
    try {
      evalLazyWrap(code, fileName);
    } catch (e) {
      logException("UJS", e);
      return false;
    }
    return true;
  }

  let libObj = {
    _dir: null,
    flist: {},
    get dir () {
      if (!this._dir) {
        var dirr = getUserLibDir();
        this._dir = dirr;
        return dirr;
      }
      return this._dir;
    },
  };

  let incObj = {
    _dir: null,
    flist: {},
    get dir () {
      if (!this._dir) {
        var dirr = getUserJSDir();
        dirr.append(nfo.name);
        this._dir = dirr;
        return dirr;
      }
      return this._dir;
    },
  };

  let includer = function (fname) {
    //debuglog("includer: ", JSON.stringify(fname));
    if (typeof(fname) === "object") {
      if (!("length" in fname)) throw new Error("string expected");
      if (fname.length == 0) return;
    } else {
      if (typeof(fname) !== "string") throw new Error("string expected");
      fname = [fname];
    }
    for (let jsfn of fname) {
      if (jsfn === undefined || jsfn === null) continue;
      if (typeof(jsfn) !== "string") throw new Error("string expected");
      if (jsfn.length == 0) continue;
      if (!(/\.js$/.test(jsfn))) jsfn += ".js";
      let fl = buildRelFile(this.dir, jsfn);
      //debuglog("loading ", fl.path);
      if (!fl || !fl.exists() || fl.isDirectory() || !fl.isReadable()) {
        throw new Error("can't import file: "+jsfn);
      }
      let path = fl.path;
      let rq = this.flist[path];
      if (rq !== undefined) {
        if (rq === false) return; // nothing to do
        throw new Error("circular import in file: "+jsfn);
      }
      this.flist[path] = true; // in progress
      let text = fileReadText(fl);
      evalLazyWrap(text, newFileURI(fl).spec, false);
      this.flist[path] = false; // done
    }
  };

  try {
    sandbox.requirelib = tieto(libObj, includer);
    sandbox.requireinc = tieto(incObj, includer);

    // eval libraries
    for (let fl of nfo.libs) {
      if (!fl.exists() || fl.isDirectory() || !fl.isReadable()) return;
      if (fl.path in reqLibs) continue; // already required
      libObj.flist[fl.path] = true;
      let text = fileReadText(fl);
      let url = newFileURI(fl).spec;
      if (!evalNoThrow(text, url)) return;
      libObj.flist[fl.path] = false;
    }

    // eval includes
    for (let fl of nfo.incs) {
      if (!fl.exists() || fl.isDirectory() || !fl.isReadable()) return;
      if (fl.path in reqFiles) continue; // already required
      incObj.flist[fl.path] = true;
      let text = fileReadText(fl);
      let url = newFileURI(fl).spec;
      if (!evalNoThrow(text, url)) return;
      incObj.flist[fl.path] = false;
    }

    // eval main script
    {
      if (!nfo.file.exists() || nfo.file.isDirectory() || !nfo.file.isReadable()) return;
      let text = fileReadText(nfo.file);
      let url = newFileURI(nfo.file).spec;
      evalNoThrow(text, url, true);
    }
  } catch (e) {
    logException("XUJS", e);
  }
};
