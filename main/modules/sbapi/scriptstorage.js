/*
 * Copyright 2004-2007 Aaron Boodman
 * Portions copyright 2015 Ketmar Dark <ketmar@ketmar.no-ip.org>
 * Contributors: See contributors list in install.rdf and CREDITS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * Note that this license applies only to the Greasemonkey extension source
 * files, not to the user scripts which it runs. User scripts are licensed
 * separately by their authors.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 */
////////////////////////////////////////////////////////////////////////////////
function ScriptStorage (nfo) {
  this.dbBaseName = nfo.name;
  this.dbObj = null;
  return this;
}


ScriptStorage.prototype.__defineGetter__("dbFile", function () {
  let file = getUserDBDir();
  file.append(this.dbBaseName+".db");
  return file;
});


ScriptStorage.prototype.__defineGetter__("db", function () {
  if (this.dbObj == null) {
    this.dbObj = Services.storage.openDatabase(this.dbFile);
    // the auto_vacuum pragma has to be set before the table is created
    //this.dbObj.executeSimpleSQL("PRAGMA auto_vacuum = INCREMENTAL;");
    //this.dbObj.executeSimpleSQL("PRAGMA incremental_vacuum(10);");
    this.dbObj.executeSimpleSQL("PRAGMA page_size = 512;");
    this.dbObj.executeSimpleSQL("PRAGMA auto_vacuum = NONE;");
    //this.dbObj.executeSimpleSQL("PRAGMA journal_mode = WAL;");
    //this.dbObj.executeSimpleSQL("PRAGMA wal_autocheckpoint = 10;");
    this.dbObj.executeSimpleSQL("PRAGMA journal_mode = DELETE;"); // will slow down bulk inserts, but IDC
    this.dbObj.executeSimpleSQL(
      "CREATE TABLE IF NOT EXISTS scriptvals ("+
      "    name TEXT PRIMARY KEY NOT NULL"+
      "  , value TEXT"+
      ")"
    );
    // run vacuum once manually to switch to the correct auto_vacuum mode for
    // databases that were created with incorrect auto_vacuum
    //this.dbObj.executeSimpleSQL("VACUUM;");
  }
  return this.dbObj;
});


ScriptStorage.prototype.__defineGetter__("opened", function () {
  return (this.dbObj != null);
});


ScriptStorage.prototype.close = function () {
  if (this.dbObj) {
    this.dbObj.close();
    this.dbObj = null;
  }
};


ScriptStorage.prototype.setValue = function (name, val) {
  if (arguments.length != 2) {
    logError("invalid number of arguments to `GM_setValue()`");
    throw new Error("invalid number of arguments to `GM_setValue()`");
  }
  if (val === null) {
    this.deleteValue(name);
    return;
  }
  //if (name === undefined) return;
  //if (typeof(name) === "Object") return;
  let stmt = this.db.createStatement("INSERT OR REPLACE INTO scriptvals (name, value) VALUES (:name, :value)");
  try {
    stmt.params.name = name;
    stmt.params.value = JSON.stringify(val);
    stmt.execute();
  } finally {
    stmt.reset();
  }
};


ScriptStorage.prototype.getValue = function (name, defval) {
  if (arguments.length < 1 || arguments.length > 2) {
    logError("invalid number of arguments to `GM_getValue()`");
    throw new Error("invalid number of arguments to `GM_getValue()`");
  }
  // removed, as suggested by https://github.com/qsniyg/
  //if (typeof(defval) === "undefined") defval = null;
  let value = null;
  let stmt = this.db.createStatement("SELECT value FROM scriptvals WHERE name=:name LIMIT 1");
  try {
    stmt.params.name = name;
    while (stmt.step()) value = stmt.row.value;
  } catch (e) {
    logException("GM_getValue", e);
  } finally {
    stmt.reset();
  }
  if (typeof(value) !== "string") return defval;
  // parse json
  try {
    let exposedProps = "__exposedProps__";
    value = JSON.parse(value);
    if (typeof(value) == "object") {
      if (!(exposedProps in value)) value[exposedProps] = {};
      for (let prop in value) {
        if (prop !== exposedProps && Object.prototype.hasOwnProperty.call(value, prop)) {
          value[exposedProps][prop] = "rw"; // there is nothing wrong in making this r/w
        }
      }
    }
    return value;
  } catch (e) {
    //logError("JSON parse error: "+e.name+": "+e.message);
    return defval;
  }
};


ScriptStorage.prototype.deleteValue = function (name) {
  if (arguments.length != 1) {
    logError("invalid number of arguments to `GM_deleteValue()`");
    throw new Error("invalid number of arguments to `GM_deleteValue()`");
  }
  let stmt = this.db.createStatement("DELETE FROM scriptvals WHERE name = :name");
  try {
    stmt.params.name = name;
    stmt.execute();
  } finally {
    stmt.reset();
  }
};


ScriptStorage.prototype.listValues = function () {
  if (arguments.length != 0) {
    logError("invalid number of arguments to `GM_listValues()`");
    throw new Error("invalid number of arguments to `GM_listValues()`");
  }
  //let count = 0;
  //let eprp = {length: "r"};
  //let vals = {};
  let valueNames = [];
  let stmt = this.db.createStatement("SELECT name FROM scriptvals");
  try {
    while (stmt.executeStep()) {
      valueNames.push(stmt.row.name);
      //vals[count++] = stmt.row.name;
    }
  } finally {
    stmt.reset();
  }
  //let vals = Array.prototype.slice.call(valueNames);
  //vals.__exposedProps__ = {length: "r"};
  //!vals.length = count;
  //!vals.__exposedProps__ = eprp;
  let vals = valueNames;
  return vals;
};


////////////////////////////////////////////////////////////////////////////////
exports.ScriptStorage = ScriptStorage;
