/* coded by Ketmar // Invisible Vector (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/txt/copying/ for more details.
 */
////////////////////////////////////////////////////////////////////////////////
exports.openTab = function (win, url, inbg) {
  let afterCurrent = null;
  if (typeof(inbg) === "object") {
    afterCurrent = ("afterCurrent" in inbg ? !!inbg.afterCurrent : null);
    inbg = ("background" in inbg ? !!inbg.background : null);
  } else if (typeof(inbg) != "boolean") {
    inbg = null;
  }
  let dwn = win.QueryInterface(Ci.nsIInterfaceRequestor).getInterface(Ci.nsIDOMWindow);
  if (!dwn) return;
  let chromeWindow =
    dwn.QueryInterface(Ci.nsIInterfaceRequestor).
    getInterface(Ci.nsIWebNavigation).
    QueryInterface(Ci.nsIDocShellTreeItem).
    rootTreeItem.
    QueryInterface(Ci.nsIInterfaceRequestor).
    getInterface(Ci.nsIDOMWindow);
  if (!chromeWindow) return;
  let bro = chromeWindow.gBrowser;
  if (!bro) return;
  let docbro = bro.getBrowserForDocument(dwn.top.document);
  if (!docbro) return;
  //let tabIdx = bro.getBrowserIndexForDocument(dwn.top.document);
  //let myTab = bro.tabs[tabIdx];
  let myTab = bro._getTabForContentWindow(dwn.top);
  let tabIdx = myTab._tPos;
  let myIsCurrent = (myTab == bro.mCurrentTab);
  let nt = bro.addTab(url, {ownerTab: myTab, relatedToCurrent: myIsCurrent});
  let bg = (inbg !== null ? inbg : Services.prefs.getBoolPref("browser.tabs.loadInBackground"));
  let rel = (afterCurrent !== null ? afterCurrent : Services.prefs.getBoolPref("browser.tabs.insertRelatedAfterCurrent"));
  if (!bg && myIsCurrent) bro.selectedTab = nt;
  if (myTab && rel) bro.moveTabTo(nt, tabIdx+1);
};
