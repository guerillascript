/*
 * Copyright 2004-2007 Aaron Boodman
 * Portions copyright 2015 Ketmar Dark <ketmar@ketmar.no-ip.org>
 * Contributors: See contributors list in install.rdf and CREDITS
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * Note that this license applies only to the Greasemonkey extension source
 * files, not to the user scripts which it runs. User scripts are licensed
 * separately by their authors.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 */
////////////////////////////////////////////////////////////////////////////////
uses("resource://gre/modules/PrivateBrowsingUtils.jsm");


////////////////////////////////////////////////////////////////////////////////
let ioSvc = Cc["@mozilla.org/network/io-service;1"].getService(Ci.nsIIOService);


function uriFromUrl (url, base) {
  let baseUri = null;
  if (typeof(base) === "string") {
    baseUri = uriFromUrl(base);
  } else if (base) {
    baseUri = base;
  }
  try {
    return ioSvc.newURI(url, null, baseUri);
  } catch (e) {
    return null;
  }
}


/*
 * Accessing windows that are closed can be dangerous after http://bugzil.la/695480
 * this routine takes care of being careful to not trigger any of those broken edge cases
 */
function windowIsClosed (aWin) {
  try {
    // if isDeadWrapper (Firefox 15+ only) tells us the window is dead
    if (Cu.isDeadWrapper && Cu.isDeadWrapper(aWin)) return true;
    // if we can access the .closed property and it is true, or there is any problem accessing that property
    try {
      if (aWin.closed) return true;
    } catch (e) {
      return true;
    }
  } catch (e) {
    logException("windowIsClosed", e);
    // failsafe: in case of any failure, destroy the command to avoid leaks
    return true;
  }
  return false;
}


////////////////////////////////////////////////////////////////////////////////
function GuerillaXmlHttpReqester (wrappedContentWin, originUrl, sandbox) {
  this.wrappedContentWin = wrappedContentWin;
  this.originUrl = originUrl;
  this.sandbox = sandbox;
  //TODO: add `Cu.getObjectPrincipal(sandbox)` when Pale Moon get this API
  this.sandboxPrincipal = null;
  //debuglog("GuerillaXmlHttpReqester created (", originUrl, ")");
  return this;
}


////////////////////////////////////////////////////////////////////////////////
// this function gets called by user scripts in content security scope to
// start a cross-domain xmlhttp request.
//
// details should look like:
// {method,url,onload,onerror,onreadystatechange,headers,data}
// headers should be in the form {name:value,name:value,etc}
// can't support mimetype because i think it's only used for forcing
// text/xml and we can't support that
GuerillaXmlHttpReqester.prototype.contentStartRequest = function (details) {
  //debuglog("GuerillaXmlHttpReqester called (", details.url, ")");
  let uri;
  try {
    // Validate and parse the (possibly relative) given URL.
    uri = uriFromUrl(details.url, this.originUrl);
    url = uri.spec;
  } catch (e) {
    // A malformed URL won't be parsed properly.
    logException("invalid url '"+details.url+"'", e);
    throw new Error("invalid url: "+details.url);
  }

  // we can check `uri.scheme` here, but i'm allowing full access,
  // 'cause guerilla scripts shouldn't be picked from arbitrary web trashsites
  let req = Cc["@mozilla.org/xmlextras/xmlhttprequest;1"].createInstance(Ci.nsIXMLHttpRequest);
  tieto(this, "chromeStartRequest", url, details, req)();

  // export properties
  let rv = {
    __exposedProps__: {
      abort: "r",
      finalUrl: "r",
      readyState: "r",
      responseHeaders: "r",
      responseText: "r",
      responseXML: "rw",
      responseJSON: "rw",
      status: "r",
      statusText: "r",
      byteLength: "r"
    },
    abort: function () { return req.abort(); },
    finalUrl: null,
    readyState: null,
    responseHeaders: null,
    responseText: null,
    responseXML: null,
    responseJSON: null,
    status: null,
    statusText: null,
    byteLength: null
  };

  if (!!details.synchronous) {
    rv.finalUrl = req.finalUrl;
    rv.readyState = req.readyState;
    rv.responseHeaders = req.getAllResponseHeaders();
    try {
      rv.responseText = req.responseText;
    } catch (e) {
      // some response types don't have .responseText (but do have e.g. blob.response): ignore
    }
    try {
      rv.responseXML = req.responseXML;
    } catch (e) {
      // some response types don't have .responseText (but do have e.g. blob.response): ignore
    }
    try {
      rv.responseJSON = req.responseJSON;
    } catch (e) {
      // some response types don't have .responseText (but do have e.g. blob.response): ignore
    }
    try {
      rv.byteLength = req.byteLength;
    } catch (e) {}
    rv.status = req.status;
    rv.statusText = req.statusText;
  }

  return rv;
};


////////////////////////////////////////////////////////////////////////////////
// this function is intended to be called in chrome's security context, so
// that it can access other domains without security warning
GuerillaXmlHttpReqester.prototype.chromeStartRequest = function (safeUrl, details, req) {
  this.setupReferer(details, req);

  let setupRequestEvent = tieto(this, "setupRequestEvent", this.wrappedContentWin, this.sandbox);

  setupRequestEvent(req, "abort", details);
  setupRequestEvent(req, "error", details);
  setupRequestEvent(req, "load", details);
  setupRequestEvent(req, "loadend", details);
  setupRequestEvent(req, "loadstart", details);
  setupRequestEvent(req, "progress", details);
  setupRequestEvent(req, "readystatechange", details);
  setupRequestEvent(req, "timeout", details);
  if (details.upload) {
    setupRequestEvent(req.upload, "abort", details.upload);
    setupRequestEvent(req.upload, "error", details.upload);
    setupRequestEvent(req.upload, "load", details.upload);
    setupRequestEvent(req.upload, "loadend", details.upload);
    setupRequestEvent(req.upload, "progress", details.upload);
    setupRequestEvent(req.upload, "timeout", details.upload);
  }

  req.mozBackgroundRequest = !!details.mozBackgroundRequest;

  req.open(details.method, safeUrl, !details.synchronous, details.user||"", details.password||"");

  let channel;

  let isPrivate = PrivateBrowsingUtils.isWindowPrivate(this.wrappedContentWin);

  if (isPrivate) {
    channel = req.channel.QueryInterface(Ci.nsIPrivateBrowsingChannel);
    channel.setPrivate(true);
  }

  channel = req.channel.QueryInterface(Ci.nsIHttpChannelInternal);
  channel.forceAllowThirdPartyCookie = true;

  if (details.overrideMimeType) req.overrideMimeType(details.overrideMimeType);
  if (details.responseType) req.responseType = details.responseType;

  if (details.timeout) req.timeout = details.timeout;

  if ("redirectionLimit" in details) {
    try {
      let httpChannel = req.channel.QueryInterface(Ci.nsIHttpChannel);
      httpChannel.redirectionLimit = details.redirectionLimit;
    } catch (e) {}
  }

  if (details.headers) {
    let headers = details.headers;
    for (let prop in headers) {
      if (Object.prototype.hasOwnProperty.call(headers, prop)) req.setRequestHeader(prop, headers[prop]);
    }
  }

  let body = (details.data ? details.data : null);
  if (details.binary) {
    req.sendAsBinary(body);
  } else {
    req.send(body);
  }
};


////////////////////////////////////////////////////////////////////////////////
// sets the "Referer" HTTP header for this GM_XHR request.
// Firefox does not let chrome JS set the "Referer" HTTP header via XHR
// directly. However, we can still set it indirectly via an
// http-on-modify-request observer.
GuerillaXmlHttpReqester.prototype.setupReferer = function (details, req) {
  if (!details.headers || !details.headers.Referer) return;
  let observerService = Cc["@mozilla.org/observer-service;1"].getService(Ci.nsIObserverService);
  let requestObserver = {};
  requestObserver.observe = function (subject, topic, data) {
    observerService.removeObserver(requestObserver, "http-on-modify-request");
    let channel = subject.QueryInterface(Ci.nsIChannel);
    if (channel == req.channel) {
      debuglog("setting referer ", details.headers.Referer);
      let httpChannel = subject.QueryInterface(Ci.nsIHttpChannel);
      httpChannel.setRequestHeader("Referer", details.headers.Referer, false);
    }
  };
  try {
    observerService.addObserver(requestObserver, "http-on-modify-request", false);
  } catch (e) {}
};


////////////////////////////////////////////////////////////////////////////////
// arranges for the specified `event` on xmlhttprequest `req` to call the
// method by the same name which is a property of `details` in the content
// window's security context.
GuerillaXmlHttpReqester.prototype.setupRequestEvent = function (wrappedContentWin, sandbox, req, event, details) {
  // Waive Xrays so that we can read callback function properties ...
  details = Cu.waiveXrays(details);
  let eventCallback = details["on"+event];
  if (!eventCallback) return;

  //TODO: add principals checking when Pale Moon get this API

  req.addEventListener(event, function (evt) {
    let responseState = {
      __exposedProps__: {
        context: "r",
        finalUrl: "r",
        lengthComputable: "r",
        loaded: "r",
        readyState: "r",
        response: "r",
        responseHeaders: "r",
        responseText: "rw",
        responseXML: "rw",
        responseJSON: "rw",
        status: "r",
        statusText: "r",
        total: "r",
        byteLength: "r"
      },
      context: details.context || null,
      finalUrl: null,
      lengthComputable: null,
      loaded: null,
      readyState: req.readyState,
      response: req.response,
      responseHeaders: null,
      responseText: null,
      responseXML: null,
      responseJSON: null,
      status: null,
      statusText: null,
      total: null,
      byteLength: null
    };

    try {
      responseState.responseText = req.responseText;
    } catch (e) {
      // some response types don't have .responseText (but do have e.g. blob.response): ignore
    }
    try {
      responseState.byteLength = req.byteLength;
    } catch (e) {}

    let responseXML = null;
    try {
      responseXML = req.responseXML;
    } catch (e) {
      // ignore failure; at least in responseType blob case, this access fails
    }
    if (responseXML) {
      // clone the XML object into a content-window-scoped document
      let xmlDoc = new wrappedContentWin.Document();
      let clone = xmlDoc.importNode(responseXML.documentElement, true);
      xmlDoc.appendChild(clone);
      responseState.responseXML = xmlDoc;
    } else {
      responseState.responseXML = "";
    }

    let responseJSON = null;
    try {
      responseJSON = req.responseJSON;
    } catch (e) {
      // ignore failure; at least in responseType blob case, this access fails
    }
    if (responseJSON) {
      responseState.responseJSON = Cu.cloneInto(responseJSON, sandbox);
    }

    switch (event) {
      case "progress":
        responseState.lengthComputable = evt.lengthComputable;
        responseState.loaded = evt.loaded;
        responseState.total = evt.total;
        break;
      case "error":
        break;
      default:
        if (4 != req.readyState) break;
        responseState.responseHeaders = req.getAllResponseHeaders();
        responseState.status = req.status;
        responseState.statusText = req.statusText;
        responseState.finalUrl = req.channel.URI.spec;
        break;
    }

    if (windowIsClosed(wrappedContentWin)) return;

    // Pop back onto browser thread and call event handler.
    // Have to use nested function here instead of GM_util.tieto because
    // otherwise details[event].apply can point to window.setTimeout, which
    // can be abused to get increased privileges.
    new XPCNativeWrapper(wrappedContentWin, "setTimeout()").setTimeout(function () { eventCallback.call(details, responseState); }, 0);
  }, false);
};


////////////////////////////////////////////////////////////////////////////////
exports.GuerillaXmlHttpReqester = GuerillaXmlHttpReqester;
