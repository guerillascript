/* coded by Ketmar // Invisible Vector (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/txt/copying/ for more details.
 */
////////////////////////////////////////////////////////////////////////////////
let wholeMetaRE = /^\s*\/\/\s*==UserScript==\s*([\s\S]*?)^\/\/\s*==\/UserScript==/m;
let metakvRE = /^\s*@(\S+)\s+(.+?)\s*$/;
let metakeyRE = /^\s*@(\S+)\s*$/;


// return array of {name, value} objects; value can be null
// special: resource value is {name, url}
function parseMeta (text) {
  let res = new Array();
  // get the stuff between ==UserScript== lines
  let mt = text.match(wholeMetaRE);
  if (!mt) return res;
  let meta = mt[1].replace(/^\s+/, "");
  for (let line of meta.split("\n")) {
    // remove slashes
    if (line.length < 2 || line[0] != "/" || line[1] != "/") continue;
    line = line.substr(2);
    let obj = {};
    let kv = line.match(metakvRE);
    if (!kv) {
      kv = line.match(metakeyRE);
      if (!kv) continue;
      obj.name = kv[1];
      obj.value = null;
    } else {
      obj.name = kv[1];
      obj.value = kv[2];
      if (kv[1] === "resource") {
        mt = kv[2].match(/^(\S+)\s+(.+)$/);
        if (mt) obj.value = {name:mt[1], url:mt[2]};
      }
    }
    res.push(obj);
  }
  // utilities
  function isNameEqual (name, pattern) {
    return (typeof(pattern) === "string" ? name == pattern : pattern.indexOf(name) >= 0);
  }
  // add methods
  // get first field with name `name`
  res.getField = function (name) {
    for (let kv of this) if (isNameEqual(kv.name, name)) return kv;
    return null;
  };
  // get last field with name `name`
  res.getLastField = function (name) {
    for (let idx = this.length-1; idx >= 0; --idx) {
      let kv = this[idx];
      if (isNameEqual(kv.name, name)) return kv;
    }
    return null;
  };
  // for each field with given name
  // if callbacks return something except undefined, null or false -- exit with this value
  // callback: {string name, string value}; value can be null for value-less fields
  res.forEachField = function (name, cb) {
    for (let kv of this) {
      if (isNameEqual(kv.name, name)) {
        let res = cb(kv);
        if (typeof(res) !== "undefined") {
          if (res !== false && res !== null) return res;
        }
      }
    }
    return undefined;
  };
  // get all fields as array of {name, value}
  res.getFields = function (name) {
    let res = new Array();
    for (let kv of this) if (isNameEqual(kv.name, name)) res.push(kv);
    return res;
  };
  return res;
}


////////////////////////////////////////////////////////////////////////////////
exports.parseMeta = parseMeta;
