/* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/GPL 2.0/LGPL 2.1
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Original Code is Page Modifications code.
 *
 * The Initial Developer of the Original Code is Mozilla.
 * Portions created by the Initial Developer are Copyright (C) 2009
 * the Initial Developer. All Rights Reserved.
 *
 * Contributor(s):
 *   David Dahl <ddahl@mozilla.com>
 *   Drew Willcoxon <adw@mozilla.com>
 *   Erik Vold <erikvvold@gmail.com>
 *   Nils Maier <maierman@web.de>
 *   Anthony Lieuallen <arantius@gmail.com>
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"), or
 * the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
 * in which case the provisions of the GPL or the LGPL are applicable instead
 * of those above. If you wish to allow use of your version of this file only
 * under the terms of either the GPL or the LGPL, and not to allow others to
 * use your version of this file under the terms of the MPL, indicate your
 * decision by deleting the provisions above and replace them with the notice
 * and other provisions required by the GPL or the LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file under
 * the terms of any one of the MPL, the GPL or the LGPL.
 *
 * ***** END LICENSE BLOCK ***** */
////////////////////////////////////////////////////////////////////////////////
let validSchemes = ["http", "https", "ftp", "file"];
let REG_HOST = /^(?:\*\.)?[^*\/]+$|^\*$|^$/;

let iosvc = Cc["@mozilla.org/network/io-service;1"].getService(Ci.nsIIOService);


////////////////////////////////////////////////////////////////////////////////
function uri2re (uri) {
  uri = uri.replace(/^\s+/, "").replace(/\s+$/, "");
  if (uri == "") return null;
  if (uri == "*") return null;
  // regexp?
  if (uri.length >= 2 && uri.charCodeAt(0) == 47 && uri.charCodeAt(uri.length-1) == 47) {
    if (uri.length < 3) return null;
    return new RegExp(uri.substring(1, uri.length-1), "i");
  }
  // convert glob to regexp
  let re = "^";
  for (let f = 0; f < uri.length; ++f) {
    switch (uri[f]) {
      case "*": re += ".*?"; break; // any, non-greedy
      case ".": case "?": case "^": case "$": case "+":
      case "{": case "}": case "[": case "]": case "|":
      case "(": case ")": case "\\":
        re += "\\"+uri[f];
        break;
      case " ": break; // ignore spaces
      default: re += uri[f]; break;
    }
  }
  re += "$";
  //if (addonOptions.debugCache) conlog("uri:["+uri+"]  re: ["+re+"]");
  return new RegExp(re, "i");
}
exports.uri2re = uri2re;


////////////////////////////////////////////////////////////////////////////////
// For the format of "pattern", see:
//   https://developer.chrome.com/extensions/match_patterns
function MatchPattern (pattern) {
  this._pattern = pattern;

  // Special case "<all_urls>".
  if (pattern === "<all_urls>") {
    this._all = true;
    this._scheme = "all_urls";
    return;
  }
  this._all = false;

  // special case wild scheme
  if (pattern[0] === "*") {
    this._wildScheme = true;
    // forge http, to satisfy the URI parser, and get a host
    pattern = "http"+pattern.substr(1);
  }

  let uri = null;
  try {
    uri = iosvc.newURI(pattern, null, null);
  } catch (e) {
    uri = null;
  }
  if (!uri) throw new Error("invalid URI in @match");

  let scheme = (this._wildScheme ? "all" : uri.scheme);
  if (scheme != "all" && validSchemes.indexOf(scheme) == -1) throw new Error("invalid scheme in @match");

  let host = uri.host;
  if (!REG_HOST.test(host)) throw new Error("invalid host in @match");

  let path = uri.path;
  if (path[0] !== "/") throw new Error("invalid path in @match");

  this._scheme = scheme;

  if (host) {
    // We have to manually create the hostname regexp (instead of using
    // GM_convert2RegExp) to properly handle *.example.tld, which should match
    // example.tld and any of its subdomains, but not anotherexample.tld.
    this._hostExpr = new RegExp("^"+
        // Two characters in the host portion need special treatment:
        //   - ". should not be treated as a wildcard, so we escape it to \.
        //   - if the hostname only consists of "*" (i.e. full wildcard),
        //     replace it with .*
        host.replace(/\./g, "\\.").replace(/^\*$/, ".*").
        // Then, handle the special case of "*." (any or no subdomain) for match
        // patterns. "*." has been escaped to "*\." by the replace above.
        replace("*\\.", "(.*\\.)?")+"$", "i");
  } else {
    // if omitted, then it means "", an alias for localhost
    this._hostExpr = /^$/;
  }
  this._pathExpr = uri2re(path);
}

MatchPattern.prototype = {
  get pattern () (""+this._pattern),
  get all () this._all,

  doMatch: function (uriSpec) {
    if (this._all) return true;
    try {
      let uri = iosvc.newURI(pattern, null, null);
      conlog(uri.path);
      if (!this._wildScheme && this._scheme != uri.scheme) return false;
      return (this._hostExpr.test(uri.host) && this._pathExpr.test(uri.path));
    } catch (e) {}
    return false; // any error? no match
  },
};
exports.MatchPattern = MatchPattern;

