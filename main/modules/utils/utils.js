/* coded by Ketmar // Invisible Vector (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/txt/copying/ for more details.
 */
////////////////////////////////////////////////////////////////////////////////
if (typeof(exportsGlobal) === "undefined") exportsGlobal = {};


////////////////////////////////////////////////////////////////////////////////
function fileReadText (file, charset) {
  if (typeof(charset) !== "string") charset = "UTF-8";
  charset = charset||"UTF-8";
  let inputStream = Cc["@mozilla.org/network/file-input-stream;1"].createInstance(Ci.nsIFileInputStream);
  inputStream.init(file, -1, -1, null);
  let scInputStream = Cc["@mozilla.org/scriptableinputstream;1"].createInstance(Ci.nsIScriptableInputStream);
  scInputStream.init(inputStream);
  let output = scInputStream.read(-1);
  scInputStream.close();
  inputStream.close();
  let converter = Cc["@mozilla.org/intl/scriptableunicodeconverter"].createInstance(Ci.nsIScriptableUnicodeConverter);
  converter.charset = charset;
  let res = converter.ConvertToUnicode(output);
  if (typeof(res) != "string") throw new Error("invalid file '"+file.path+"'");
  // fuck BOM
  if (charset == "UTF-8" && res.length >= 3 && res.substr(0, 3) == "\u00EF\u00BB\u00BF") res = res.substr(3);
  return res;
}
exportsGlobal.fileReadText = fileReadText;
