/* coded by Ketmar // Invisible Vector (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/txt/copying/ for more details.
 */
////////////////////////////////////////////////////////////////////////////////
this.EXPORTED_SYMBOLS = [
  "addSignalListener",
  "removeSignalListener",
  "emitSignal",
  "signalNamePrefix"
];

////////////////////////////////////////////////////////////////////////////////
const {utils:Cu, classes:Cc, interfaces:Ci, results:Cr} = Components;


function genUUID () {
  let uuidgen = Cc["@mozilla.org/uuid-generator;1"].createInstance(Ci.nsIUUIDGenerator);
  if (!uuidgen) throw new Error("no UUID generator available!");
  return uuidgen.generateUUID().toString().replace(/[^-a-z0-9]/ig, "");
}


////////////////////////////////////////////////////////////////////////////////
const signalNamePrefix = "signal-"+genUUID()+"-";
const obs = Cc["@mozilla.org/observer-service;1"].getService(Ci.nsIObserverService);


////////////////////////////////////////////////////////////////////////////////
// string key: name
// object value:
//   object observer
//   array[] callbacks
let observers = {};

function addSignalListener (name, cback) {
  if (typeof(name) !== "string" || !name) throw new Error("invalid signal name");
  if (typeof(cback) !== "function") throw new Error("callback function expected");
  // check if already here
  let nfo = observers[name];
  if (nfo !== undefined) {
    for (let cb of nfo.callbacks) if (cb === cback) return; // nothing to do
  } else {
    // no observer, create one
    nfo = {callbacks:[]};
    nfo.observer = {
      observe: function (subject, topic, data) {
        topic = topic.substr(signalNamePrefix.length); // remove prefix
        if (data && data.length) {
          try { data = JSON.parse(data); } catch (e) { Cu.reportError(e); return; }
        } else {
          data = null;
        }
        for (let cb of nfo.callbacks) {
          try { cb(topic, data); } catch (e) { Cu.reportError(e); }
        }
      },
    };
    obs.addObserver(nfo.observer, signalNamePrefix+name, false);
    observers[name] = nfo;
  }
  nfo.callbacks.push(cback);
}


function removeSignalListener (name, cback) {
  if (typeof(name) !== "string" || !name) throw new Error("invalid signal name");
  if (typeof(cback) !== "function") throw new Error("callback function expected");
  // find observer
  let nfo = observers[name];
  if (nfo !== undefined) {
    for (let [idx, cb] of Iterator(nfo.callbacks)) {
      if (cb === cback) {
        // remove callback
        nfo.callbacks.splice(idx, 1);
        // if there's no more callbacks, remove observer
        if (nfo.callbacks.length === 0) {
          obs.removeObserver(nfo.observer, signalNamePrefix+name);
          delete observers[name];
        }
        return;
      }
    }
  }
}


function emitSignal (name, data) {
  if (typeof(name) !== "string" || !name) throw new Error("invalid signal name");
  if (!(name in observers)) return; // no listeners -- no need to send anything
  data = (typeof(data) === "undefined" ? null : (data !== null ? JSON.stringify(data) : null));
  obs.notifyObservers(null, signalNamePrefix+name, data);
}
