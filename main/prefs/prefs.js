/* coded by Ketmar // Invisible Vector (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/txt/copying/ for more details.
 */
////////////////////////////////////////////////////////////////////////////////
var PREFS_BRANCH = "extensions.guerilla.";

var PREFS = {
  fsoReadmeViewed: false, // readme viewed?
  debugMode: false,
  debugCache: false,
  logEnabled: true,
  active: true,
  // all pathes are relative to profile dir
  jsdir: "guerillajs/scripts",
  libdir: "guerillajs/libs",
  dbdir: "guerillajs/data",
  pkgdir: "guerillajs/packages",
  pkgdbdir: "guerillajs/packages/_0data",
  pkgtempdir: "guerillajs/packages/_0temp",
};

var PREFS_DIR_NAMES = {
  jsdir: "JSDir",
  libdir: "LibDir",
  dbdir: "DBDir",
  pkgdir: "PkgDir",
  pkgdbdir: "PkgDBDir",
  pkgtempdir: "PkgTempDir",
};


////////////////////////////////////////////////////////////////////////////////
// restore old values if user has some old dirs
/*
(function () {
  var PREFS_OLD_DIRS = {
    jsdir: "guerillajs",
    libdir: "guerillajs_libs",
    dbdir: "guerillajs_data",
    pkgdir: "guerillajs_packages",
    pkgdbdir: "guerillajs_packages/_0data",
    pkgtempdir: "guerillajs_packages/_0temp",
  };

  const dirSvcProps = Cc["@mozilla.org/file/directory_service;1"].getService(Ci.nsIProperties);
  let oldFound = false;
  for (let [n, v] of Iterator(PREFS_OLD_DIRS)) {
    if (n === "jsdir" || v === "guerillajs") continue;
    // get profile directory
    let df = dirSvcProps.get("ProfD", Ci.nsIFile);
    if (v.length) for (let d of v.split("/")) if (d.length) df.append(d);
    if (df.exists()) {
      Cu.reportError("GS: old directory ('", n, "' [", v, "]) found!");
      oldFound = true;
      break;
    }
  }
  // found? restore old values
  if (oldFound) for (let [n, v] of Iterator(PREFS_OLD_DIRS)) PREFS[n] = v;
})();
*/
