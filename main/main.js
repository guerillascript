/* coded by Ketmar // Invisible Vector (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/txt/copying/ for more details.
 */
// main extension loader -- user-defined code
require("utils/utils");


var {PackageDB} = require("pkg/packagedb");
var pkgDB = new PackageDB();

registerStartupHook("packagedb creation", function () {
  (function () { let db = pkgDB.db; })();
});

registerShutdownHook("packagedb closing", function () {
  //pkgDB.close();
  pkgDB = null;
});

var {PkgDownloader} = require("pkg/downpkg");
var scacheAPI = require("scriptcache");
var pkgman = require("pkg/pkgman");

require("injector");
require("concmd");


var {openTab} = require("sbapi/opentab");
registerWindowLoadHook("readme hook", function (win) {
  // open documentation on first run
  if (!addonOptions.fsoReadmeViewed) {
    //addonOptions.fsoReadmeViewed = true;
    //conlog("win=", win, "; url=", gsdoxUrl+"README.txt");
    //openTab(win, gsdoxUrl+"README.txt", false);
    let dwn = win.QueryInterface(Ci.nsIInterfaceRequestor).getInterface(Ci.nsIDOMWindow);
    if (!dwn) { conlog("000"); return; }
    let chromeWindow =
      dwn.QueryInterface(Ci.nsIInterfaceRequestor).
      getInterface(Ci.nsIWebNavigation).
      QueryInterface(Ci.nsIDocShellTreeItem).
      rootTreeItem.
      QueryInterface(Ci.nsIInterfaceRequestor).
      getInterface(Ci.nsIDOMWindow);
    if (!chromeWindow) { conlog("001"); return; }
    let bro = chromeWindow.gBrowser;
    if (!bro) { conlog("002"); return; }
    //conlog("***");

    let oss = Cc["@mozilla.org/observer-service;1"].getService(Ci.nsIObserverService);
    let observer = {
      observe: function (aSubject, aTopic, aData) {
        //conlog("OBS: ", aTopic);
        if (aTopic === "sessionstore-windows-restored") {
          if (!addonOptions.fsoReadmeViewed) {
            bro.selectedTab = bro.addTab(gsdoxUrl+"README.txt");
            addonOptions.fsoReadmeViewed = true;
          }
          //oss.removeObserver(observer, "sessionstore-windows-restored");
        }
      },
    };
    oss.addObserver(observer, "sessionstore-windows-restored", false);
  }
});
