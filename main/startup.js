/* coded by Ketmar // Invisible Vector (psyc://ketmar.no-ip.org/~Ketmar)
 * Understanding is not required. Only obedience.
 *
 * This program is free software. It comes without any warranty, to
 * the extent permitted by applicable law. You can redistribute it
 * and/or modify it under the terms of the Do What The Fuck You Want
 * To Public License, Version 2, as published by Sam Hocevar. See
 * http://www.wtfpl.net/txt/copying/ for more details.
 */
////////////////////////////////////////////////////////////////////////////////
this.EXPORTED_SYMBOLS = [];

let {utils:Cu, classes:Cc, interfaces:Ci, results:Cr} = Components;


////////////////////////////////////////////////////////////////////////////////
// addonName: used in `alert()`
function initAddonInternal (global, addonName, urlMain, urlJS) {
  const {utils:Cu, classes:Cc, interfaces:Ci, results:Cr} = Components;

  const promptSvc = Cc["@mozilla.org/embedcomp/prompt-service;1"].getService(Ci.nsIPromptService);
  const appInfo = Cc["@mozilla.org/xre/app-info;1"].getService(Ci.nsIXULAppInfo);
  switch (appInfo.ID) {
    case "{8de7fcbb-c55c-4fbe-bfc5-fc555c87dbc4}": break;
    case "{ec8030f7-c20a-464f-9b0e-13a3a9e97384}": promptSvc.alert(null, addonName, "Ff is not supported"); return null;
    case "{3550f703-e582-4d05-9a08-453d09bdfdc6}": promptSvc.alert(null, addonName, "Thunderbird is not supported"); return null;
    case "{92650c4d-4b8e-4d2a-b7eb-24ecf4f6b63a}": promptSvc.alert(null, addonName, "Seamonkey is not supported"); return null;
    default: promptSvc.alert(null, addonName, "Unknown application "+appInfo.ID+" is not supported"); return null;
  }

  // bootstrapped addons has no `window`
  //const principal = (typeof(window) !== "undefined" ? window : Cc["@mozilla.org/systemprincipal;1"].createInstance(Ci.nsIPrincipal));
  let gsbox = new Cu.Sandbox(Cc["@mozilla.org/systemprincipal;1"].createInstance(Ci.nsIPrincipal), {
    sandboxName: addonName+" Internal Sandbox",
    wantComponents: false,
    wantXrays: false,
    wantXHRConstructor: true,
  });
  Cu.import("resource://gre/modules/Services.jsm", gsbox);

  // bootstrapped addons has no `window`
  if (typeof(window) !== "undefined") {
    gsbox.window = window;
    gsbox.document = window.document;
  }

  for (let name of ["CSS","indexedDB","XMLHttpRequest","TextEncoder","TextDecoder","URL","URLSearchParams","atob","btoa","Blob","File"]) {
    if (name in global) {
      //Components.utils.reportError("exporting '"+name+"'");
      gsbox[name] = global[name];
    }
  }

  // add some common API
  Object.defineProperty(gsbox, "jscodeUrl", {get: function () urlJS});
  Object.defineProperty(gsbox, "contentUrl", {get: function () urlMain});

  // `tieto` API: ties method to object, so it always has correct `this`
  // you may specify additional arguments that will be always passed unchanged
  function tieto (obj, method) {
    let mt;
    if (typeof(obj) === "undefined") throw new Error("object or `null` expected");
    if (obj !== null && typeof(obj) !== "object") throw new Error("object or `null` expected");
    switch (typeof(method)) {
      case "string":
        if (!obj) throw new Error("can't take method by name from `null` object");
        if (!(method in obj)) throw new Error("method '"+method+"' doesn't exist in object '"+obj+"'");
        mt = obj[method];
        if (typeof(mt) !== "function") throw new Error("method '"+method+"' isn't a function in object '"+obj+"'");
        break;
      case "function":
        if (!obj) obj = {};
        mt = method;
        break;
      default:
        throw new Error("`method` should be function or method name");
    }
    // hack: replace first array item; avoiding extra arrays
    let rest = Array.prototype.splice.call(arguments, 1, arguments.length);
    rest[0] = obj;
    // now use `bind`
    return mt.bind.apply(mt, rest);
  }

  const sb_tieto = tieto(gsbox, tieto);
  Object.defineProperty(gsbox, "tieto", {get: function () sb_tieto});

  const sb_Components = tieto(gsbox, function (c) c, Components);
  const sb_Cu = tieto(gsbox, function (cu) cu, Cu);
  const sb_Cc = tieto(gsbox, function (cc) cc, Cc);
  const sb_Ci = tieto(gsbox, function (ci) ci, Ci);
  const sb_Cr = tieto(gsbox, function (cr) cr, Cr);

  Object.defineProperty(gsbox, "Components", {get: function () sb_Components()});
  Object.defineProperty(gsbox, "Cu", {get: function () sb_Cu()});
  Object.defineProperty(gsbox, "Cc", {get: function () sb_Cc()});
  Object.defineProperty(gsbox, "Ci", {get: function () sb_Ci()});
  Object.defineProperty(gsbox, "Cr", {get: function () sb_Cr()});

  // console log functions
  function logErr (str) Cu.reportError(str);
  const sb_logError = tieto(gsbox, function (logErrMsg) {
    if (arguments.length > 1) {
      let s = "";
      for (let idx = 1; idx < arguments.length; ++idx) s += ""+arguments[idx];
      logErrMsg(s);
    }
  }, logErr);
  Object.defineProperty(gsbox, "logError", {get: function () sb_logError});

  const sb_logException = tieto(gsbox, function (logErrMsg, msg, e) {
    if (typeof(e) === "undefined" && typeof(msg) === "object") {
      e = msg;
      msg = "SOME";
    }
    if (typeof(e) !== "undefined" /*&& e instanceof global.Error*/ && e) {
      logErrMsg(""+msg+" ERROR: "+e.name+": "+e.message+" : "+e.lineNumber);
      if (e.stack) logErrMsg(e.stack);
      logErrMsg(e);
    } else {
      logErrMsg(""+msg);
      if (typeof(e) !== "undefined" && e) logErrMsg("e="+e);
    }
  }, logErr);
  Object.defineProperty(gsbox, "logException", {get: function () sb_logException});

  // https://developer.mozilla.org/en-US/docs/XPCOM_Interface_Reference/nsIConsoleService#logStringMessage() - wstring / wide string
  const conService = Cc["@mozilla.org/consoleservice;1"].getService(Ci.nsIConsoleService);
  function logStr (str) conService.logStringMessage(str);

  const sb_conlog = tieto(gsbox, function (logStrMsg) {
    if (arguments.length > 1) {
      let le = ("addonOptions" in this ? this["addonOptions"].logEnabled : true);
      if (!le) return;
      let s = "";
      for (let idx = 1; idx < arguments.length; ++idx) s += ""+arguments[idx];
      logStrMsg(s);
    }
  }, logStr);
  Object.defineProperty(gsbox, "conlog", {get: function () sb_conlog});

  const sb_debuglog = tieto(gsbox, function (logStrMsg) {
    if (arguments.length > 1) {
      if ("addonOptions" in this) {
        let ao = this["addonOptions"];
        //if (("logEnabled" in ao) && !ao.logEnabled) return;
        if (!("debugMode" in ao) || !ao.debugMode) return;
      }
      let s = "";
      for (let idx = 1; idx < arguments.length; ++idx) s += ""+arguments[idx];
      logStrMsg(s);
    }
  }, logStr);
  Object.defineProperty(gsbox, "debuglog", {get: function () sb_debuglog});

  const sb_print = tieto(gsbox, function (logStrMsg) {
    if (arguments.length > 1) {
      let s = "";
      for (let idx = 1; idx < arguments.length; ++idx) {
        let t = ""+arguments[idx];
        if (t) s += " "+t;
      }
      logStrMsg(s.substr(1));
    }
  }, logStr);
  Object.defineProperty(gsbox, "print", {get: function () sb_print});

  // script loaders
  const newURI = Cc["@mozilla.org/network/io-service;1"].getService(Ci.nsIIOService).newURI;
  //const newFileURI = Cc["@mozilla.org/network/io-service;1"].getService(Ci.nsIIOService).newFileURI;
  const loadSubScript = Cc["@mozilla.org/moz/jssubscript-loader;1"].getService(Ci.mozIJSSubScriptLoader).loadSubScript;

  const specsSchemes = [
    { re: /^mainjs:(.+)$/, pfx: urlJS},
    { re: /^main:(.+)$/, pfx: urlMain}
  ];
  function toFullUrl (scriptSpec, name) {
    if (/^(?:chrome|resource|file):/.test(name)) return name;
    for (let sp of specsSchemes) {
      let mt = name.match(sp.re);
      if (mt) return sp.pfx+mt[1];
    }
    return scriptSpec+name;
  }

  // include("incname")
  const sb_include = tieto(gsbox, function (scriptSpec, newURI, loadSubScript, reportError, name) {
    if (name.length == 0) throw new Error("invalid include name: '"+name+"'");
    if (!(/.\.js$/.test(name))) name += ".js";
    let fullUrl = toFullUrl(scriptSpec, name);
    try {
      let uri = newURI(fullUrl, null, null);
      loadSubScript(uri.spec, gsbox, "UTF-8");
    } catch (e) {
      if (typeof(e) === "object") {
        reportError("INCLUDE ERROR'"+name+"': "+e.name+": "+e.message+" : "+e.lineNumber+"\n"+e.stack);
        reportError(e);
      } else {
        reportError("INCLUDE ERROR '"+name+"': "+e);
      }
      throw e;
    }
  }, urlJS+"includes/", newURI, loadSubScript, Cu.reportError);
  Object.defineProperty(gsbox, "include", {get: function () sb_include});


  function loadTextContentsFromUrl (url, encoding) {
    if (typeof(encoding) !== "string") encoding = "UTF-8"; else encoding = encoding||"UTF-8";

    function toUnicode (text) {
      if (typeof(text) === "string") {
        if (text.length == 0) return "";
      } else if (text instanceof ArrayBuffer) {
        if (text.byteLength == 0) return "";
        text = new Uint8Array(text);
        return new TextDecoder(encoding).decode(text);
      } else if ((text instanceof Uint8Array) || (text instanceof Int8Array)) {
        if (text.length == 0) return "";
        return new TextDecoder(encoding).decode(text);
      } else {
        return "";
      }
      let converter = Cc["@mozilla.org/intl/scriptableunicodeconverter"].createInstance(Ci.nsIScriptableUnicodeConverter);
      converter.charset = encoding;
      let res = converter.ConvertToUnicode(text);
      if (res.length >= 3 && res.substr(0, 3) == "\u00EF\u00BB\u00BF") res = res.substr(3); // fuck BOM
      return res;
    }

    // download from URL
    let req = Cc["@mozilla.org/xmlextras/xmlhttprequest;1"].createInstance();
    req.mozBackgroundRequest = true;
    req.open("GET", url, false);
    req.timeout = 30*1000;
    const cirq = Ci.nsIRequest;
    req.channel.loadFlags |= cirq.INHIBIT_CACHING|cirq.INHIBIT_PERSISTENT_CACHING|cirq.LOAD_BYPASS_CACHE|cirq.LOAD_ANONYMOUS;
    req.responseType = "arraybuffer";
    //if (sendCookies) addCookies(req, urlsend);
    let error = false;
    req.onerror = function () { error = true; }; // just in case
    req.ontimeout = function () { error = true; }; // just in case
    req.send(null);
    if (!error) {
      error = (req.status != 0 && Math.floor(req.status/100) != 2);
      //(/^file:/.test(file.spec) ? (req.status != 0) : (Math.floor(req.status/100) != 2));
    }
    if (error) throw new Error("can't load URI contents: status="+req.status+"; error="+error);
    return toUnicode(req.response, encoding);
  }


  let modules = {}; // list of loaded modules

  // require("modname")
  const sb_require = tieto(gsbox, function (scriptSpec, newURI, loadSubScript, reportError, import_, modules, name) {
    if (name.length == 0) throw new Error("invalid include name: '"+name+"'");
    if (!(/.\.js$/.test(name))) name += ".js";
    if (name in modules) return modules[name];
    let fullUrl = toFullUrl(scriptSpec, name);
    //else sb_debuglog("requiring '", name, "' : [", fullUrl, "]");
    //let scope = {};
    let scope = Object.create(gsbox);
    scope.exports = {};
    scope.exportsGlobal = {};

    try {
      let uri = newURI(fullUrl, null, null);

      let box = new Cu.Sandbox(Cc["@mozilla.org/systemprincipal;1"].createInstance(Ci.nsIPrincipal), {
        wantComponents: false,
        wantXrays: false,
        wantXHRConstructor: true,
        sandboxPrototype: scope,
      });

      // uses(name[, name]...);
      let sc_uses = tieto(box, function (scope, import_) {
        if (arguments.length > 3) {
          for (let idx = 2; idx < arguments.length; ++idx) import_(arguments[idx], scope);
          return null;
        } else if (arguments.length == 3) {
          return import_(arguments[2], scope);
        } else {
          return null;
        }
      }, scope, Cu.import);
      Object.defineProperty(box, "uses", {get: function () sc_uses});

      // include("incname")
      let sc_include = tieto(box, function (scriptSpec, newURI, loadSubScript, reportError, box, name) {
        if (name.length == 0) throw new Error("invalid include name: '"+name+"'");
        if (!(/.\.js$/.test(name))) name += ".js";
        let fullUrl = toFullUrl(scriptSpec, name);
        try {
          let uri = newURI(fullUrl, null, null);
          loadSubScript(uri.spec, box, "UTF-8");
        } catch (e) {
          if (typeof(e) === "object") {
            reportError("INCLUDE ERROR'"+name+"': "+e.name+": "+e.message+" : "+e.lineNumber+"\n"+e.stack);
            reportError(e);
          } else {
            reportError("INCLUDE ERROR '"+name+"': "+e);
          }
          throw e;
        }
      }, urlJS+"includes/", newURI, loadSubScript, Cu.reportError, box);
      Object.defineProperty(box, "include", {get: function () sc_include});

      let text = loadTextContentsFromUrl(uri.spec);
      Cu.evalInSandbox(text, box, "ECMAv5", name, 1);
    } catch (e) {
      if (typeof(e) === "object") {
        reportError("REQUIRE ERROR'"+name+"': "+e.name+": "+e.message+" : "+e.lineNumber+"\n"+e.stack);
        reportError(e);
      } else {
        reportError("REQUIRE ERROR '"+name+"': "+e);
      }
      throw e;
    }

    modules[name] = scope.exports;
    if (typeof(scope.exportsGlobal) === "object") {
      for (let k in scope.exportsGlobal) {
        if (k !== "__exposedProps__" && Object.prototype.hasOwnProperty.call(scope.exportsGlobal, k)) {
          try { gsbox[k] = scope.exportsGlobal[k]; } catch (e) {}
        }
      }
    }
    return scope.exports;
  }, urlJS+"modules/", newURI, loadSubScript, Cu.reportError, Cu.import, modules);
  Object.defineProperty(gsbox, "require", {get: function () sb_require});


  // alert("msg")
  let sb_alert = tieto(gsbox, function (promptSvc, addonName, msg) {
    promptSvc.alert(null, addonName, ""+msg);
  }, promptSvc, addonName);
  Object.defineProperty(gsbox, "alert", {get: function () sb_alert});


  let startupHooks = [], shutdownHooks = [];
  let winloadHooks = [], winunloadHooks = [];

  let mainLoaded = false;

  function runHooks (hooktype, list, rev, firsttime) {
    let rest = Array.prototype.splice.call(arguments, 4, arguments.length);
    let callmain = false;
    if (firsttime) {
      //Cu.reportError("runHooks ("+hooktype+"): first time");
      if (!mainLoaded) {
        //Cu.reportError("runHooks ("+hooktype+"): main not loaded");
        // now load main file
        mainLoaded = true;
        let loadmain = ("__dontLoadMainJS" in global ? !global.__dontLoadMainJS : true);
        if (loadmain) {
          //Cu.reportError("runHooks ("+hooktype+"): loading main...");
          //Cu.reportError("runHooks ("+hooktype+"): len="+list.length);
          gsbox.include("mainjs:prefs/prefs.js"); // define default extension preferences
          gsbox.require("mainjs:prefsengine.js");
          callmain = true;
          //Cu.reportError("runHooks ("+hooktype+"): len="+list.length);
          gsbox.include("mainjs:main.js");
        }
      }
    }
    if (hooktype === "shutdown" && typeof(gsbox.main_preshutdown) === "function") {
      try {
        gsbox.main_preshutdown();
      } catch (e) {
        Cu.reportError("PRESHUTDOWN ERROR: "+e.name+": "+e.message+" : "+e.lineNumber);
        if (e.stack) Cu.reportError(e.stack);
        Cu.reportError(e);
      }
    }
    let idx = (rev ? list.length : -1);
    for (;;) {
      if (rev) {
        if (--idx < 0) break;
      } else {
        if (++idx >= list.length) break;
      }
      let h = list[idx];
      try {
        //conService.logStringMessage("running hook: '"+h.name+"'");
        let args = Array.prototype.slice.call(rest);
        //Array.prototype.push.apply(args, arguments);
        h.cback.apply(gsbox, args);
      } catch (e) {
        callmain = false;
        Cu.reportError("HOOK '"+hooktype+"' ("+h.name+") ERROR: "+e.name+": "+e.message+" : "+e.lineNumber+(e.stack ? "\n"+e.stack : ""));
        Cu.reportError(e);
        if (e.stack) Cu.reportError(e.stack);
      }
    }
    if (callmain && typeof(gsbox.main_postinit) === "function") gsbox.main_postinit();
  }

  function addHook (hooks, name, cback) {
    if (typeof(cback) === "undefined") {
      // register*Hook(hookfn)
      cback = name;
      name = null;
    }
    if (typeof(name) === "undefined") throw new Error("name or `null` expected");
    if (!name) name = "";
    if (typeof(name) !== "string") throw new Error("name or `null` expected");
    if (typeof(cback) !== "function") throw new Error("function expected");
    for (let obj in hooks) if (obj.cback === cback) return; // nothing to do
    hooks.push({name:name, cback:cback});
  }

  const sb_rsth = tieto(gsbox, addHook, startupHooks);
  const sb_rsuh = tieto(gsbox, addHook, shutdownHooks);

  // registerStartupHook(name, hookfn)
  // registerShutdownHook(name, hookfn)
  Object.defineProperty(gsbox, "registerStartupHook", {get: function () sb_rsth});
  Object.defineProperty(gsbox, "registerShutdownHook", {get: function () sb_rsuh});

  const sb_rwlh = tieto(gsbox, addHook, winloadHooks);
  const sb_rwuh = tieto(gsbox, addHook, winunloadHooks);

  // registerWindowLoadHook(name, hookfn)
  // registerWindowUnloadHook(name, hookfn)
  Object.defineProperty(gsbox, "registerWindowLoadHook", {get: function () sb_rwlh});
  Object.defineProperty(gsbox, "registerWindowUnloadHook", {get: function () sb_rwuh});

  return {
    sbox: gsbox,
    tieto: tieto,
    defProp: tieto(this, function (mainobj, name, ops) {
      //Cu.reportError("GS: defining property '"+name+"'");
      mainobj.defineProperty(gsbox, name, ops);
    }, Object),
    //onInstall: function () {},
    //onUnistall: function () {},
    onStartup: tieto(this, runHooks, "startup", startupHooks, false, true),
    onShutdown: tieto(this, runHooks, "shutdown", shutdownHooks, true, false),
    onWindowLoad: tieto(this, runHooks, "window opened", winloadHooks, true, false),
    onWindowUnload: tieto(this, runHooks, "window closed", winunloadHooks, true, false),
  };
}


////////////////////////////////////////////////////////////////////////////////
const addonName = "Guerilla Script"; // addon name, used in `alert()`
const addonContentUrl = "chrome://guerilla-script/content/"; // content URL, must end with "/"
const addonJSUrl = "chrome://guerilla-script-jscode/content/"; // addon js code URL, must end with "/"

let addonobj = null;


////////////////////////////////////////////////////////////////////////////////
function initAddon (global) {
  try {
    addonobj = initAddonInternal(global, addonName, addonContentUrl, addonJSUrl);
    if (!addonobj) throw new Error("can't init addon '"+addonName+"'"); // alas, something is wrong
    addonobj.defProp("gsdoxUrl", {get: function () "chrome://guerilla-script-dox/content/"});
    addonobj.onStartup();
  } catch (e) {
    if (addonobj) try { addonobj.onShutdown(); } catch (xx) {}
    addonobj = false;
    Cu.reportError(e.stack);
    Cu.reportError(e);
    throw e;
  }
}


////////////////////////////////////////////////////////////////////////////////
let windowCount = 0;

function newWindow (global, win) {
  if (addonobj === null) initAddon(global);
  if (addonobj) {
    win.addEventListener("load", function () {
      ++windowCount;
      // setup unload hook
      global.window.addEventListener("unload", function () {
        --windowCount;
        addonobj.onWindowUnload(win);
        if (windowCount == 0) {
          // no more registered windows --> shutdown
          addonobj.onShutdown();
        }
      }, false);
      addonobj.onWindowLoad(win);
    }, false);
  }
}


////////////////////////////////////////////////////////////////////////////////
function getAddonObj () addonobj;
