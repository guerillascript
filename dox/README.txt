What is Guerilla Scripting?
===========================

GS is a userscript injecting engine, slightly similar to GreaseMonkey,
but its primary audience are knowledgeable power users. GS was modelled
after Opera 12 userscript engine, but with support for some GreaseMonkey
API.

GS has absolutely no GUI controls.

GS also don't have any l10n, and probably will not have any. As there is
no GUI and no user-visible dialogs, I don't see any real value in adding
l10n to GS.

And last thing to say here: I know that it should be written "guerrilla".
but do you really expect that guerrillas know how to spell such bizarre
words? I found spelling error long after I released a preview to several
people, and the name stuck. Let's say that one "r" is a trademark, like
"ckr" for those you-know-them hipster sites.


How Guerilla Scripting works?
=============================

Contrary to GreaseMonkey, GS doesn't need any registration of userscripts.
Simply copy your script in GS scripts directory with your file manager,
and GS should find it. To remove script, simply delete it or change its
extension to someting that is not ".js".

There is no itegration with any sites, nor automatic script updates from
teh internets. GS will *never* connect to any internet site on it's own,
it doesn't do any automatic installation or updating of userscripts.


Is Guerilla Scripting secure?
=============================

The only way to add a userscript to GS (besides packages) is to MANUALLY
copy it in GS scripts directory (which is in your profile dir, named
"guerillajs/scripts"). So if you wrote all the scripts by yourself, or
got them from a friend whom you can trust, there should be no security
risks.

GS will not do any connections to any sites on it's own. But note that
userscripts are free to connect to any site they want (exactly as in
GreaseMonkey), and additionally, userscripts are free to read you local
files too. Please note that GreaseMonkey userscripts can't read your
local files (they can try, but the request will fail), so you shouldn't
simply copy GreaseMonkey userscripts to GS without understanging what
they are doing under the hood.

The reason for giving GS userscripts access to local files is the
difference in addon target group. While GreaseMonkey is aimed at casual
users, GS is aimed at knowledgeable power users, who may want to squeeze
some more power from their userscripts.

WARNING! You should NEVER EVER install userscripts downloaded from
internet to GS, unless you know *for* *sure* what those scripts do, why
and how. If you'll install some malicious script downloaded from internet,
that script can breach your privacy, steal or alter your private data and
so on.

Please note that GreaseMonkey scripts can do privacy violation to some
extent too, that is nature of userscripts.


API differences to GreaseMonkey
===============================

some GreaseMonkey API aren't implemented, and some implemented in
slightly different way. Script @metas altered too.

API that should work the same in GS and GreaseMonkey:
  unsafeWindow (not really, it is more a hack)
  GM_log
  GM_getValue
  GM_setValue
  GM_deleteValue
  GM_listValues
  GM_xmlhttpRequest
  GM_addStyle
  GM_openInTab(url[, background]) -- if `background` is boolean

extended API:
  GM_openInTab(url, options)
  options is an object which can contain such fields:
    bool background -- open tab in background
    bool afterCurrent -- open tab after current one,
                         if current is userscript tab

API that doesn't work (i.e. exists, but does nothing):
  GM_getResourceText
  GM_getResourceURL
  GM_registerMenuCommand
  GM_setClipboard

additional API:
  conlog (same as GM_log)
  logError (logs error message to error console)
  GM_generateUUID (same as in Scriptish)
  GM_cryptoHash (same as in Scriptish)
  GM_safeHTMLParser (same as in Scriptish)


requirelib(name) API
====================

`requirelib` includes file from GS library directory, if that file wasn't
included yet.


requireinc(name) API
====================

`requireinc` includes file from userscript includes directory (see
@require), if that file wasn't included yet.


@meta differences to GreaseMonkey
=================================

GS understands very small amount of @metas, and simply skips all keywords
it doesn't understand.

working:
  @include
  @exclude
  @noframe
  @run-at

different:
  @require -- this works COMPLETELY different, see below


Changed @meta: @require
=======================

@require now can require only js files. All js files you want to @require
should be placed in the directory named after your script. Let me give a
sample.

Let's assume you has script named "myscript.js" with such contents:

// ==UserScript==
// @require somecode.js
// ==/UserScript==

GS will look for file "./myscript/somecode.js", relative to "myscript.js"
directory.

If any @required file is absent, GS will refuse to run such userscript.

Multiple @require for same file are allowed, but that file will be loaded
only once, first time GS sees @require with it.


Known bugs
==========

Some GM API is not supported.

pjaxed pages doesn't work right.

Probably many other bugs I didn't found yet.


Good luck, and happy hacking!
Remember, by downloading FOSS software you are downloading COMMUNISM!
Ketmar Dark  <ketmar@ketmar.no-ip.org>
